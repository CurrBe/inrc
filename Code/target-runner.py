import argparse
import logging
import sys

from block_mip import solve_block_scen

def model_instance(w6, w7, w8, DATFILE, instance, seed):
    """
    Function that solves a model instance given a set of weights as well as a seed value. This is used for parameter racing weights using IRACE

    Parameters
    ----------
    w6 : int
        Cost coefficient for linear component of CS62
    w7 : int
        Cost coefficient for CS72
    w8 : int
        Cost coefficient for CS8
    DATFILE : str   
        File that we write the cost (and only the cost) to
    instance : str  
        Name of the instance solved in the form scen_hist_weeknums.txt
    seed : int
        Random seed value passed to Gurobi
    """    
    sep_path = instance.strip().split('/')
    info = sep_path[-1].strip().split('_')
    scen = info[0]
    print('Scen = ', scen)
    hist_num = int(info[1])
    week_nums = [int(x) for x in info[2].strip().split('-')]
    
    tot_cost, avRuntime, tineout = solve_block_scen(scen, hist_num, week_nums, \
        w6=w6, w7=w7, w8=w8, w6q=0.0, w11=w11, seed=seed)
    with open(DATFILE, 'w') as f:
        f.write(str(tot_cost))
    
if __name__ == '__main__':
    # This part defines the windows executable used by IRACE in parameter racing
    with open('args.txt', 'w') as f:
        f.write(str(sys.argv))
    
    ap = argparse.ArgumentParser(description='Training for extension objective weight values for block MIP model')
    ap.add_argument("-v", "--verbose", help="increase output verbosity", action="store_true")
    # 5 args for extension weights
    ap.add_argument('--w6', dest = 'w6', type=float, required=True, help='S6* linear weight value')
    ap.add_argument('--w7', dest='w7', type=float, required=True, help='S7* weight value')
    ap.add_argument('--w8', dest='w8', type=float, required=True, help='S8* weight value')
    
    # 1 arg for file name to save and load output value
    ap.add_argument('--datfile', dest='datfile', type=str, required=True, help='File where it will be save the roster cost (result)')

    ap.add_argument('-i', dest='instance', type=str, required=True, help='File containing instances to be solved')
    ap.add_argument('--seed', dest='seed', type=int, required=True, help='Seed value passed to gurobi solver')

    args = ap.parse_args()
    logging.debug(args)

    #call the model instance function and pass it the args
    # model_instance(args.w6, args.w7, args.w8, args.w6q, args.w11, args.scen, args.histnum, args.weeknums, args.datfile)
    # model_instance(args.w6, args.w7, args.w8, args.w6q, args.w11, args.datfile, args.instance, args.seed)
    model_instance(args.w6, args.w7, args.w8, args.datfile, args.instance, args.seed)