from gurobipy import *
import os
from tabulate import tabulate
from random import randint
import win32com.client as win32

from data import DATAPATH, read_scenario
from block_mip import solve_block_scen, solve_block_week
from initMIP import solve_scen, solve_week
from block_generation import gen_master


def test_block(filename, w6 = 7.8841, w7 = 14.3991, w8 = 19.9534, w6q = 0.3321):
    """
    Benchmark the performance of the block model by running 10 tests for each instance, results given for each instance so rank can be calculated

    Parameters
    ----------
    filename : str
        Name of the file containing instance names

    w6 : float, optional
        Cost coefficient of the linear part of CS62, by default 17.9550

    w7 : float, optional
        Cost coefficient of CS72, by default 15.9860

    w8 : float, optional
        Cost coefficient of CS8, by default 16.5245
    """    

    # set up for email notifications on progress
    outlook = win32.Dispatch('outlook.application')
    mail = outlook.CreateItem(0)
    mail.To = "CurrBe98@outlook.com"
    mail.Subject = "INRC-II Benchmarking Progress Report"
    mail.Body = "Starting Execution of Benchmarking"
    mail.Send()
    

    # open instance file and load in the instances to solve
    path = os.path.join(DATAPATH, 'Benchmarking', filename)
    with open(path, 'r') as f:
        lines = f.readlines()

    # initialise lists to record results- best case and average for quality and 
    # runtime
    best_qual = [0] * (len(lines) - 1)
    best_time = [0] * (len(lines) - 1)
    av_qual = [0] * (len(lines) - 1)
    av_time = [0] * (len(lines) - 1)

    trial_obj_path = os.path.join(DATAPATH, 'Benchmarking', 'TrialResults.txt')
    table_data_path = os.path.join(DATAPATH, 'Benchmarking', 'TableResults.txt')

    s = 'Av\t Best\n'
    with open(table_data_path, 'w') as f:
        f.writelines(s)
    # loop over each instance and perform 10 trials
    for i in range(len(lines) - 2, len(lines) - 1):
        instance = lines[1:][i]
        print('INSTANCE: ', instance)
        mail = outlook.CreateItem(0)
        mail.To = "CurrBe98@outlook.com"
        mail.Subject = "INRC-II Benchmarking Progress Report"
        mail.Body = "Starting instance {}/{}: {}".format(i - 9, len(lines) - 12, instance)
        mail.Send()
        dest_file = instance + 'Results.txt'
        dat = instance.strip().split('_')
        print(dat)
        scen = dat[0]
        hist_num = int(dat[1])
        week_nums = [int(x) for x in dat[2].strip().split('-')]
        seeds = [0] * 10
        times = [0] * 10
        quals = [0] * 10
        s = 'Trial \t Cost \t Runtime \t Seed\n'
        s += '---------------------------------------\n'
        for trial in range(10):
            print('TRIAL: ', trial)
            seed = randint(0, 2000000000)
            seeds[trial] = seed
            tot_cost, runtime, timeout = solve_block_scen(scen, hist_num, week_nums, w6=w6, w7=w7, w8=w8, w6q=w6q, seed=seed)
            times[trial] = runtime
            quals[trial] = tot_cost
            s += '{} \t {} \t {} \t {}\n'.format(trial, quals[trial], times[trial], seed)
        best_qual[i] = min(quals)
        best_time[i] = min(times)
        av_qual[i] = sum(quals)/len(quals)
        av_time[i] = sum(times)/len(times)
        s += 'Av Cost\t Av Time \t Best Cost \t Best Time\n'
        s += '-------------------------------------\n'
        s += '{} \t {} \t {} \t {}\n'.format(av_qual[i], av_time[i], best_qual[i], best_time[i])

        # write results to a text file for future reference
        with open(os.path.join(DATAPATH, 'Benchmarking', dest_file), 'w') as f:
            f.write(s)

        trial_str = ''
        for trial in range(10):
            trial_str += '{}\n'.format(quals[trial])
        table_str = ''
        for trial in range(10):
            table_str += '{} {}\n'.format(av_qual, av_time)
        with open(trial_obj_path, 'a') as f:
            f.write(table_str)
    
    mail = outlook.CreateItem(0)
    mail.To = "CurrBe98@outlook.com"
    mail.Subject = "INRC-II Benchmarking Progress Report"
    mail.Body = "Execution Done!"
    mail.Send()

    
def run_tests():
    """
    Test both models on a subset of the hidden instances
    """    
    scen_names = ['n035w4', 'n035w8', 'n070w4', 'n070w8', 'n110w4', 'n110w8']
    hist_nums = [0, 1, 2]
    week_nums_n035w4 = [[[1, 7, 1, 8], 
                        [4, 2, 1, 6]],
                        [[0, 6, 9, 2]],
                        [[8, 8, 7, 5],
                        [9, 9, 2, 1]]]

    week_nums_n035w8 = [[[6, 2, 9, 8, 7, 7, 9, 8]],
                        [[0, 8, 1, 6, 1, 7, 2, 0]],
                        [[8, 8, 7, 5, 0, 0, 6, 9]]]

    week_nums_n070w4 = [[[3, 6, 5, 1]],
                        [[1, 3, 8, 8]],
                        [[3, 5, 8, 2]]]

    week_nums_n070w8 = [[[3, 3, 9, 2, 3, 7, 5, 2]],
                        [[9, 8, 9, 9, 2, 8, 1, 4]],
                        [[4, 9, 2, 9, 2, 7, 0, 6]]]

    week_nums_n110w4 = [[[1, 9, 3, 5]],
                        [[0, 1, 6, 4]],
                        [[9, 8, 4, 9]]]

    week_nums_n110w8 = [[[2, 1, 1, 7, 2, 6, 4, 7]],
                        [[0, 6, 1, 0, 3, 2, 9, 1]],
                        [[8, 5, 7, 3, 9, 8, 8, 5]]]

    week_nums = {(scen, hist): [] for scen in scen_names for hist in range(3)}

    week_nums[0, 0] = week_nums_n035w4[0]
    week_nums[0, 1] = week_nums_n035w4[1]
    week_nums[0, 2] = week_nums_n035w4[2]
    week_nums[1, 0] = week_nums_n035w8[0]
    week_nums[1, 1] = week_nums_n035w8[1]
    week_nums[1, 2] = week_nums_n035w8[2]
    week_nums[2, 0] = week_nums_n070w4[0]
    week_nums[2, 1] = week_nums_n070w4[1]
    week_nums[2, 2] = week_nums_n070w4[2]
    week_nums[3, 0] = week_nums_n070w8[0]
    week_nums[3, 1] = week_nums_n070w8[1]
    week_nums[3, 2] = week_nums_n070w8[2]
    week_nums[4, 0] = week_nums_n110w4[0]
    week_nums[4, 1] = week_nums_n110w4[1]
    week_nums[4, 2] = week_nums_n110w4[2]
    week_nums[5, 0] = week_nums_n110w8[0]
    week_nums[5, 1] = week_nums_n110w8[1]
    week_nums[5, 2] = week_nums_n110w8[2]

    table = []

    filepath = os.path.join(DATAPATH, 'Benchmarking')
    if not os.path.isdir(filepath):
        os.mkdir(filepath)

    qual_list = []
    time_list = []
    for scen in range(3, len(scen_names)):
        for hist in hist_nums:
            for instance in (week_nums[scen, hist]):
                row_base = ['Mischek & Misliu', scen_names[scen]]
                row_block = ['Block Formulation', scen_names[scen]]
                row_base.append(hist)
                row_block.append(hist)
                week_str = ''
                for week in instance:
                        week_str += '{} '.format(str(week))
                row_base.append(week_str)
                row_block.append(week_str)
                print('\n\n')
                print(("############ BASE ################", scen, hist, instance))
                print('\n\n')
                tot_cost_base, avRuntime_base, timeout_base = solve_scen(scen_names[scen], hist, instance)
                timeout_base_str = ''
                found = False
                for w in range(len(timeout_base)):
                    if timeout_base[w] == 1:
                        timeout_base_str += str(w) + ' '
                        found = True
                if not found:
                    timeout_base_str = 'None'
                
                row_base.append(tot_cost_base)
                row_base.append(avRuntime_base)
                row_base.append(timeout_base_str)

                print('\n\n')
                print(("########### BLOCK ###############", scen, hist, instance))
                print('\n\n')
                tot_cost_block, avRuntime_block, timeout_block = solve_block_scen(scen_names[scen], hist, instance, w6=13.4171, w7=18.3878, w8=13.4394, w11=0)#, w11=13.2117)
                qual_list.append((scen, hist, week_nums, 'AvQ', av_qual, 'AvT', av_time, 'BQ', best_trial_qual, 'BT', best_trial_time))
                timeout_block_str = ''
                found = False
                for w in range(len(timeout_block)):
                    if timeout_block[w] == 1:
                        timeout_block_str += str(w) + ' '
                        found = True
                if not found:
                    timeout_block_str = 'None'
                
                row_block.append(tot_cost_block)
                row_block.append(avRuntime_block)
                row_block.append(timeout_block_str)
                
                table.append(row_base)
                table.append(row_block)

                tab = tabulate(table, ["Method", "Scenario", "History Data", "Week Data", "Total Cost", "Average Runtime", "Timeout Weeks"], tablefmt = 'grid')

                with open(os.path.join(filepath, 'Benchmarks.txt'), 'w') as f:
                    f.writelines(tab)

                with open(os.path.join(filepath, 'BenchmarksTrials.txt'), 'w') as f:
                    f.writelines(qual_list)


def main():
    filename = 'Instances.txt' 
    try:
        test_block(filename)
    except Exception as ex:
        outlook = win32.Dispatch('outlook.application')
        mail = outlook.CreateItem(0)
        mail.To = "CurrBe98@outlook.com"
        mail.Subject = "INRC-II Benchmarking Progress Report"
        template = "An exception of type {0} occurred. Arguments:\n{1!r}"
        message = template.format(type(ex).__name__, ex.args)
        mail.Body = message
        mail.Send()
        raise(ex)
            
if __name__ == '__main__':
    main()