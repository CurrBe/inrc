import os
import math

from data import DATAPATH, read_hist, read_week

import os
import math

from data import DATAPATH, read_hist

def eval_roster(scen, hist_num, week_nums, path, filename, mode, XV, YV, N, S, K, W, D, \
    coverage, banned_seq, nurse_skills, shift_details, shift_names, \
    shift_blocks, break_blocks, requests, complete_weekend, tot_assignments, tot_weekends, trial = -1):
    """
        Evaluate the quality of a roster from a given solution file

    Parameters
    ----------
    scen : str
        Name of scenario
    hist_num : int
        Number of the history file used for initial W0 history
    week_nums : list
        List of week numbers that define weekly demand cases
    path : str
        
    filename : Name of the where the results will be stored in
        Name of the folder where results of evaluation will be stored
    mode : str
        Indicates whether we start the evaluation using Base history files or Block
    XV : dict
        Dictionary of x values from model
    YV : dict
        Dictionary of y valyes from model
    N : list
        Set of nurses
    S : list
        Set of shifts
    K : list
        Set of skills
    W : list
        Set of weeks
    D : list
        Set of days
    banned_seq : list
        List of banned sequences of shift pairs
    nurse_skills : dict
        Dictionary mapping each nurse to a set of skills that nurse posesses
    shift_details : dict
        Dictionary mapping each shift to a list containing the maximum and minimum length of stretches of that shift
    shift_names : list
        List of string names for each shift
    break_blocks : list
        List of min/max number of consecutive breaks for each nurse
    requests : dict
        Dictionary mapping a nurse, shift and day to 1 if requested shift on day off, 0 otherwise
    complete_weekend : dict
        Dictionary mapping a nurse to 1 if they have the complete weekend constraint, 0 otherwise
    tot_assignments : list  
        List of each nurses total assignments so far
    tot_weekends : list
        List of each nurses total worked weekends so far
    trial : int, optional
        Trial number, can be used to label individual trial result files, by default -1

    Returns
    -------
    float
        Total cost of the roster

    Raises
    ------
    FileNotFoundError
        Invalid file path given
    """    
    H1 = [True] * len(W)
    H2 = [True] * len(W)
    H3 = [True] * len(W)
    H4 = [True] * len(W)

    S1 = [0] * len(W)
    S2 = [0] * len(W)
    S3 = [0] * len(W)
    S4 = [0] * len(W)
    S5 = [0] * len(W)

    S6 = 0
    S7 = 0

    SW = S[:-1]

    for w in W:
        if mode == 'Base':
            if w == 0 or trial < 0:
                hist_name = 'H{}-{}Base'.format(w, scen)
            else:
                hist_name = 'H{}-{}BaseT{}'.format(w, scen, trial)
        elif mode == 'Block':
            if w == 0 or trial < 0:
                hist_name = 'H{}-{}Block'.format(w, scen)
            else:
                hist_name = 'H{}-{}BlockT{}'.format(w, scen, trial)
        tot_ass, tot_weekend, shift_hist, shift_len, work_len, break_len = read_hist(path, hist_name, S, N, shift_names)
        for d in D:
            for s in SW:
                for k in K:
                    for n in N:
                        if k not in nurse_skills[n] and XV[w][n, s, k, d] > 0.9:
                            H4[w] = False
                            break
                    below_opt = max(coverage[w][s, k, d][1] - sum(XV[w][n, s, k, d] for n in N), 0)
                    S1[w] += 30 * below_opt
                    below_min = max(sum(XV[w][n, s, k, d] for n in N) - coverage[w][s, k, d][0], 0)
                    if below_min == 0:
                        H2[w] = True
                for n in N:
                    if requests[w][n, s, d] == 1 and sum(XV[w][n, s, k, d] for k in K) > 0.9:
                        S4[w] += 10
        for n in N:
            if complete_weekend[n] and round(sum(XV[w][n, s, k, 5] + XV[w][n, s, k, 6] for s in SW for k in K)) == 1:
                S5[w] += 30

        for d in D:
            for n in N:
                for (s, ss) in banned_seq:
                    if d < len(D) - 1 and sum(XV[w][n, s, k, d] for k in K) + sum(XV[w][n, ss, k, d + 1] for k in K) == 2:
                        H3[w] = False
                if sum(XV[w][n, s, k, d] for s in SW for k in K) > 1.1:
                    H1[w] = False

        for n in N:
            for d in D:
                if d == 0:
                    if sum(XV[w][n, s, k, d] for s in SW for k in K) > 0.9:
                        done_work = False
                        count_work = 1
                        while not done_work:
                            if d + count_work < len(D) and sum(XV[w][n, s, k, d + count_work] for s in SW for k in K) > 0.9:
                                count_work += 1
                            else:
                                done_work = True
                        if work_len[n] > 0:
                            c = 30 * (max(shift_blocks[n][0] - count_work - work_len[n], 0) + \
                                            max(count_work + work_len[n] - shift_blocks[n][1], 0))
                            S2[w] += c
                        else:
                            c1= 30 * max(break_blocks[n][0] - break_len[n], 0)
                            c2 = 30 * (max(shift_blocks[n][0] - count_work, 0) + \
                                            max(count_work - shift_blocks[n][1], 0))
                            S3[w] += c1
                            S2[w] += c2
                    elif sum(XV[w][n, s, k, d] for s in SW for k in K) < 0.1:
                        done_break = False
                        count_break = 1
                        while not done_break:
                            if d + count_break < len(D) and sum(XV[w][n, s, k, d + count_break] for s in SW for k in K) < 0.1:
                                count_break += 1
                            else:
                                done_break = True
                        if break_len[n] > 0:
                            c = 30 * (max(break_blocks[n][0] - count_break - break_len[n], 0) + \
                                            max(count_break + break_len[n] - break_blocks[n][1], 0))
                            S3[w] += c
                        else:
                            c1 = 30 * (max(break_blocks[n][0] - count_break, 0) + \
                                        max(count_break - break_blocks[n][1], 0))
                            c2 = 30 * (max(shift_blocks[n][0] - work_len[n], 0)) + \
                                        15 * max(shift_details[shift_hist[n]][0] - shift_len[n], 0) 
                            S3[w] += c1
                            S2[w] += c2
                    for s in SW:
                        if sum(XV[w][n, s, k, 0] for k in K) > 0.9:
                            shift_done = False
                            count_shift = 1
                            while not shift_done:
                                if count_shift < len(D) and sum(XV[w][n, s, k, count_shift] for k in K) > 0.9:
                                    count_shift += 1
                                else:
                                    shift_done = True
                            if s == shift_hist[n]:
                                c = 15 * (max(shift_details[s][0] - count_shift - shift_len[n], 0) + \
                                                max(count_shift + shift_len[n] - shift_details[s][1], 0))
                                S2[w] += c
                            elif s != shift_hist[n] and shift_hist[n] != len(SW):
                                c = 15 * (max(shift_details[s][0] - count_shift, 0) + \
                                                    max(count_shift - shift_details[s][1], 0)) + \
                                            15 * max(shift_details[shift_hist[n]][0] - shift_len[n], 0)
                                S2[w] += c
                            elif shift_hist[n] == len(SW):
                                if count_shift < len(D):
                                    c = 15 * (max(shift_details[s][0] - count_shift, 0) + \
                                                        max(count_shift - shift_details[s][1], 0))
                                    S2[w] += c
                                else:
                                    S2[w] += max(count_shift - shift_details[s][1], 0)
                else:
                    if sum(XV[w][n, s, k, d - 1] for s in SW for k in K) < 0.1 and sum(XV[w][n, s, k, d] for s in SW for k in K) > 0.9:
                        done_work = False
                        count_work = 1
                        while not done_work:
                            if d + count_work < len(D) and sum(XV[w][n, s, k, d + count_work] for s in SW for k in K) > 0.9:
                                count_work += 1
                            else:
                                done_work = True
                        if d + count_work < len(D): 
                            c = 30 * (max(shift_blocks[n][0] - count_work, 0) + \
                                            max(count_work - shift_blocks[n][1], 0))
                            S2[w] += c
                        else:
                            c = 30 * max(count_work - shift_blocks[n][1], 0)
                            S2[w] += c
                    elif sum(XV[w][n, s, k, d - 1] for s in SW for k in K) > 0.9 and sum(XV[w][n, s, k, d] for s in SW for k in K) < 0.1:
                        done_break = False
                        count_break = 1
                        while not done_break:
                            if d + count_break < len(D) and sum(XV[w][n, s, k, d + count_break] for s in SW for k in K) < 0.1:
                                count_break += 1
                            else:
                                done_break = True
                        if d + count_break < len(D):
                            c =  30 * (max(break_blocks[n][0] - count_break, 0) + \
                                            max(count_break - break_blocks[n][1], 0))
                            S3[w] += c
                        else:
                            c = 30 * max(count_break - break_blocks[n][1], 0)
                            S3[w] += c
                    for s in SW:
                        if sum(XV[w][n, s, k, d - 1] for k in K) < 0.1 and sum(XV[w][n, s, k, d] for k in K) > 0.9:
                            shift_done = False
                            count_shift = 1
                            while not shift_done:
                                if d + count_shift < len(D) and sum(XV[w][n, s, k, d + count_shift] for k in K) > 0.9:
                                    count_shift += 1
                                else:
                                    shift_done = True
                            if d + count_shift < len(D): 
                                c = 15 * (max(shift_details[s][0] - count_shift, 0) + \
                                                max(count_shift - shift_details[s][1], 0))
                                S2[w] += c
                            else:
                                c = 15 * max(count_shift - shift_details[s][1], 0)
                                S2[w] += c

    for n in N:
        num_assigned = sum(XV[w][n, s, k, d] for w in W for s in SW for k in K for d in D)
        S6 += 20 * (max(tot_assignments[n][0] - num_assigned, 0) + max(num_assigned - tot_assignments[n][1], 0))
        num_weekends = 0
        for w in W:
            if YV[w][n] > 0.9:
                num_weekends += 1
        S7 += 30 * max(num_weekends - tot_weekends[n], 0)
                    
    tot_cost = round(sum(S1[w] + S2[w] + S3[w] + S4[w] + S5[w] for w in W) + S6 + S7)
    sol_folder = 'Solution to H_{}-WD_{}'.format(hist_num, week_nums[0])
    for i in range(1, len(week_nums)):
        sol_folder += '-{}'.format(week_nums[i])

    filepath = os.path.join(DATAPATH, scen, 'Rep Sols', sol_folder)
    if not os.path.isdir(filepath):
        raise FileNotFoundError('Invalid Path!')
    with open(os.path.join(filepath, filename), 'a') as f:
        lines = '\n\n' + '-' * 24 + '\n'
        lines += 'EVALUATION SCORE\n\n'
        lines += '#TOTAL EVAL COST: {}#\n'.format(round(tot_cost))
        lines += '-Hard Constraints-\n'
        lines += 'H1: '
        for w in W:
            lines += str(H1[w]) + ' '
        lines += '\nH2: '
        for w in W:
            lines += str(H2[w]) + ' '
        lines += '\nH3: '
        for w in W:
            lines += str(H3[w]) + ' '
        lines += '\nH4: '
        for w in W:
            lines += str(H4[w]) + ' '
        
        lines += '\n-Soft Constraints-\nS1: '
        for w in W:
            lines += str(round(S1[w])) + ' '
        lines += '\nS2: '
        for w in W:
            lines += str(round(S2[w])) + ' '
        lines += '\nS3: '
        for w in W:
            lines += str(round(S3[w])) + ' '
        lines += '\nS4: '
        for w in W:
            lines += str(round(S4[w])) + ' '
        lines += '\nS5: '
        for w in W:
            lines += str(round(S5[w])) + ' '
        lines += '\nS6: {}'.format(round(S6))
        lines += '\nS7: {}\n'.format(round(S7))

        f.writelines(lines)

        return tot_cost



def eval_roster_file(scen, hist_num, week_nums, path, mode, N, S, W, K, D, \
    banned_seq, nurse_skills, shift_details, shift_names, nurse_names, shift_blocks, skills, \
    break_blocks, complete_weekend, tot_assignments, tot_weekends, compare = '', compare_start = -1):
    H1 = [True] * len(W)
    H2 = [True] * len(W)
    H3 = [True] * len(W)
    H4 = [True] * len(W)

    S1 = [0] * len(W)
    S2 = [0] * len(W)
    S3 = [0] * len(W)
    S4 = [0] * len(W)
    S5 = [0] * len(W)

    S6 = 0
    S7 = 0

    SW = S[:-1]

    # print('SW = ', SW)
    # print('S = ', S)

    X = {w: {(n, s, k, d): 0 for n in N for s in S for k in K for d in D} for w in W}
    Y = {w: {n: 0 for n in N} for w in W}

    num_ass = [0] * len(W)

    # print('NS', nurse_names)
    # print('K', skills)

    day_names = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

    # print("W = ", W)

    for w in W:
        # print('week = ', w)
        # hist_name = 'H{}-{}'.format(w, scen)
        # tot_ass, tot_weekend, shift_hist, shift_len, work_len, break_len = read_hist(path, hist_name, S, N, shift_names)

        if mode == 'Base':
            if compare == 'Block':
                if w < compare_start:
                    filename = 'W{}- SolutionBase.txt'.format(w)
                else:
                    filename = 'W{}- SolutionBlock.txt'.format(w)
                hist_filename = 'H{}-{}Block'.format(w, scen)
            else:
                filename = 'W{}- SolutionBase.txt'.format(w)
                hist_filename = 'H{}-{}Base'.format(w, scen)
            tot_ass, tot_weekend, shift_hist, shift_len, work_len, break_len = read_hist(path, hist_filename, S, N, shift_names)
            
        elif mode == 'Block':
            if compare == 'Base':
                if w <= compare_start:
                    filename = 'W{}- SolutionBase.txt'.format(w)
                    hist_filename = 'H{}-{}Base'.format(w, scen)
                else:
                    filename = 'W{}- SolutionBlock.txt'.format(w)
                    hist_filename = 'H{}-{}Block'.format(w, scen)
                
            else:
                filename = 'W{}- SolutionBlock.txt'.format(w)
                hist_filename = 'H{}-{}Block'.format(w, scen)
            tot_ass, tot_weekend, shift_hist, shift_len, work_len, break_len = read_hist(path, hist_filename, S, N, shift_names)
        # print('Tot ass: ', tot_ass)
        # print('Tot weekend: ', tot_weekend)
        # print('Shift hist: ', shift_hist)
        # print('Work len: ', work_len)
        # print('Break len: ', break_len)
        with open(os.path.join(path, filename), 'r') as f:
            raw_data = f.readlines()
        num_ass[w] = raw_data[0].strip().split()[-1]
        # print(raw_data)
        for i in range(1, len(raw_data)):
            nurse_assignment_data = raw_data[i].strip().split()
            # print(nurse_assignment_data)
            n = nurse_names.index(nurse_assignment_data[0])
            d = day_names.index(nurse_assignment_data[1])
            s = shift_names.index(nurse_assignment_data[2])
            k = skills.index(nurse_assignment_data[3])
            # print(('W', w, 'N', n, 'S', s, 'K', k, 'D', d))

            X[w][n, s, k, d] = 1

            if d == 5 or d == 6:
                Y[w][n] = 1
        # print('\n')
        week_file_name = "WD-{}-{}".format(scen, week_nums[w])
        coverage, requests = read_week(scen, path, week_file_name, SW, K, N, shift_names, skills, nurse_names)
        
        # print(shift_hist)
        for d in D:
            for s in SW:
                for k in K:
                    # for n in N:
                    #     if k not in nurse_skills[n] and (n, s, k, d) in X and X[w][n, s, k, d] > 0.9:
                    #         H4[w] = False
                    #         break
                    below_opt = max(coverage[s, k, d][1] - sum(X[w][n, s, k, d] for n in N), 0)
                    S1[w] += 30 * below_opt
                    # below_min = max(sum(X[w][n, s, k, d] for n in N) - coverage[s, k, d][0], 0)
                    # if below_min == 0:
                    #     H2[w] = True
                for n in N:
                    if requests[n, s, d] == 1 and sum(X[w][n, s, k, d] for k in nurse_skills[n]) > 0.9:
                        S4[w] += 10
        for n in N:
            if complete_weekend[n] and round(sum(X[w][n, s, k, 5] + X[w][n, s, k, 6] for s in SW for k in nurse_skills[n])) == 1:
                S5[w] += 30

        # for d in D:
        #     for n in N:
        #         for (s, ss) in banned_seq:
        #             if d < len(D) - 1 and sum(X[w][n, s, k, d] for k in K) + sum(X[w][n, ss, k, d + 1] for k in K) == 2:
        #                 H3[w] = False
        #         if sum(X[w][n, s, k, d] for s in SW for k in K) > 1.1:
        #             H1[w] = False

        for n in N:
            for d in D:
                if d == 0:
                    if sum(X[w][n, s, k, d] for s in SW for k in nurse_skills[n]) > 0.9:
                        done_work = False
                        count_work = 1
                        while not done_work:
                            if d + count_work < len(D) and sum(X[w][n, s, k, d + count_work] for s in SW for k in nurse_skills[n]) > 0.9:
                                count_work += 1
                            else:
                                done_work = True
                        if work_len[n] > 0:
                            # if w == 3:
                                # print(('CW', 'n', n, 'wl', work_len[n], 'cw', count_work))
                            c = 30 * (max(shift_blocks[n][0] - count_work - work_len[n], 0) + \
                                            max(count_work + work_len[n] - shift_blocks[n][1], 0))
                            S2[w] += c
                            # print(('S2OFW', 'w', w, 'n', n, 'C', c))
                        else:
                            c1= 30 * max(break_blocks[n][0] - break_len[n], 0)
                            c2 = 30 * (max(shift_blocks[n][0] - count_work, 0) + \
                                            max(count_work - shift_blocks[n][1], 0))
                            S3[w] += c1
                            S2[w] += c2
                            # print(('S3OF1', 'w', w, 'n', n, 'C', c1))
                            # print(('S2OF1B', 'w', w, 'n', n, 'C', c2))
                    elif sum(X[w][n, s, k, d] for s in SW for k in nurse_skills[n]) < 0.1:
                        done_break = False
                        count_break = 1
                        while not done_break:
                            if d + count_break < len(D) and sum(X[w][n, s, k, d + count_break] for s in SW for k in nurse_skills[n]) < 0.1:
                                count_break += 1
                            else:
                                done_break = True
                        if break_len[n] > 0:
                            c = (max(break_blocks[n][0] - count_break - break_len[n], 0) + \
                                            max(count_break + break_len[n] - break_blocks[n][1], 0))
                            S3[w] += c
                            # print(('S3OF', 'w', w, 'n', n, 'C', c))
                        else:
                            c1 = 30 * (max(break_blocks[n][0] - count_break, 0) + \
                                        max(count_break - break_blocks[n][1], 0))
                            c2 = 30 * (max(shift_blocks[n][0] - work_len[n], 0)) + \
                                        15 * max(shift_details[shift_hist[n]][0] - shift_len[n], 0) 
                            S3[w] += c1
                            S2[w] += c2
                            # print(('S3OF2', 'w', w, 'n', n, 'C', c1))
                            # print(('S2OF2B', 'w', w, 'n', n, 'C', c2))
                    for s in SW:
                        if sum(X[w][n, s, k, 0] for k in nurse_skills[n]) > 0.9:
                            shift_done = False
                            count_shift = 1
                            while not shift_done:
                                if count_shift < len(D) and sum(X[w][n, s, k, count_shift] for k in nurse_skills[n]) > 0.9:
                                    count_shift += 1
                                else:
                                    shift_done = True
                            if s == shift_hist[n]:
                                c = 15 * (max(shift_details[s][0] - count_shift - shift_len[n], 0) + \
                                                max(count_shift + shift_len[n] - shift_details[s][1], 0))
                                S2[w] += c
                                # print(('S2OF1', 'w', w, 'n', n, 's', s, 'C', c))
                            elif s != shift_hist[n] and shift_hist[n] != len(SW):
                                c = 15 * (max(shift_details[s][0] - count_shift, 0) + \
                                                    max(count_shift - shift_details[s][1], 0)) + \
                                            15 * max(shift_details[shift_hist[n]][0] - shift_len[n], 0)
                                S2[w] += c
                                # print(('S2OF2', 'w', w, 'n', n, 's', s, 'sh', shift_hist[n], 'C', c))
                            elif shift_hist[n] == len(SW):
                                if count_shift < len(D):
                                    c = 15 * (max(shift_details[s][0] - count_shift, 0) + \
                                                        max(count_shift - shift_details[s][1], 0))
                                    S2[w] += c
                                    # print(('S2OF3', 'w', w, 'n', n, 's', s, 'C', c))
                                else:
                                    S2[w] += max(count_shift - shift_details[s][1], 0)
                                    # print(('S2OF4', 'w', w, 'n', n, 's', s, 'C', c))
                else:
                    if sum(X[w][n, s, k, d - 1] for s in SW for k in nurse_skills[n]) < 0.1 and sum(X[w][n, s, k, d] for s in SW for k in nurse_skills[n]) > 0.9:
                        done_work = False
                        count_work = 1
                        while not done_work:
                            if d + count_work < len(D) and sum(X[w][n, s, k, d + count_work] for s in SW for k in nurse_skills[n]) > 0.9:
                                count_work += 1
                            else:
                                done_work = True
                        if d + count_work < len(D):
                            c = 30 * (max(shift_blocks[n][0] - count_work, 0) + \
                                            max(count_work - shift_blocks[n][1], 0))
                            S2[w] += c
                            # print(('S2W', 'w', w, 'n', n, 'd', d, 'C', c))
                        else:
                            c = 30 * max(count_work - shift_blocks[n][1], 0)
                            S2[w] += c
                            # print(('S2WE', 'w', w, 'n', n, 'd', d, 'C', c))
                    elif sum(X[w][n, s, k, d - 1] for s in SW for k in nurse_skills[n]) > 0.9 and sum(X[w][n, s, k, d] for s in SW for k in nurse_skills[n]) < 0.1:
                        done_break = False
                        count_break = 1
                        while not done_break:
                            if d + count_break < len(D) and sum(X[w][n, s, k, d + count_break] for s in SW for k in nurse_skills[n]) < 0.1:
                                count_break += 1
                            else:
                                done_break = True
                        if d + count_break < len(D):
                            c =  30 * (max(break_blocks[n][0] - count_break, 0) + \
                                            max(count_break - break_blocks[n][1], 0))
                            S3[w] += c
                            # print(('S3', 'w', w, 'n', n, 'd', d, 'C', c))
                        else:
                            c = 30 * max(count_break - break_blocks[n][1], 0)
                            S3[w] += c
                            # print(('S3E', 'w', w, 'n', n, 'd', d, 'C', c))
                    for s in SW:
                        if sum(X[w][n, s, k, d - 1] for k in nurse_skills[n]) < 0.1 and sum(X[w][n, s, k, d] for k in nurse_skills[n]) > 0.9:
                            shift_done = False
                            count_shift = 1
                            while not shift_done:
                                if d + count_shift < len(D) and sum(X[w][n, s, k, d + count_shift] for k in nurse_skills[n]) > 0.9:
                                    count_shift += 1
                                else:
                                    shift_done = True
                            if d + count_shift < len(D): # or w == W[-1]:
                                c = 15 * (max(shift_details[s][0] - count_shift, 0) + \
                                                max(count_shift - shift_details[s][1], 0))
                                S2[w] += c
                                # print(('S2S', 'w', w, 'n', n, 'd', d, 's', s, 'C', c))
                            else:
                                c = 15 * max(count_shift - shift_details[s][1], 0)
                                S2[w] += c
                                # print(('S2SE', 'w', w, 'n', n, 'd', d, 's', s, 'C', c))

    for n in N:
        num_assigned = sum(X[w][n, s, k, d] for w in W for s in SW for k in nurse_skills[n] for d in D)
        S6 += 20 * (max(tot_assignments[n][0] - num_assigned, 0) + max(num_assigned - tot_assignments[n][1], 0))
        num_weekends = 0
        for w in W:
            if Y[w][n] > 0.9:
                num_weekends += 1
        S7 += 30 * max(num_weekends - tot_weekends[n], 0)
                    
    tot_cost = round(sum(S1[w] + S2[w] + S3[w] + S4[w] + S5[w] for w in W) + S6 + S7)
    sol_folder = 'Solution to H_{}-WD_{}'.format(hist_num, week_nums[0])
    for i in range(1, len(week_nums)):
        sol_folder += '-{}'.format(week_nums[i])

    filepath = os.path.join(DATAPATH, scen, 'Rep Sols', sol_folder)
    if not os.path.isdir(filepath):
        raise FileNotFoundError('Invalid Path!')
    # with open(os.path.join(filepath, filename), 'a') as f:
    lines = '\n\n' + '-' * 24 + '\n'
    lines += 'EVALUATION SCORE\n\n'
    lines += '#TOTAL EVAL COST: {}#\n'.format(round(tot_cost))
    lines += '-Hard Constraints-\n'
    lines += 'H1: '
    for w in W:
        lines += str(H1[w]) + ' '
    lines += '\nH2: '
    for w in W:
        lines += str(H2[w]) + ' '
    lines += '\nH3: '
    for w in W:
        lines += str(H3[w]) + ' '
    lines += '\nH4: '
    for w in W:
        lines += str(H4[w]) + ' '
    
    lines += '\n-Soft Constraints-\nS1: '
    for w in W:
        lines += str(round(S1[w])) + ' '
    lines += '\nS2: '
    for w in W:
        lines += str(round(S2[w])) + ' '
    lines += '\nS3: '
    for w in W:
        lines += str(round(S3[w])) + ' '
    lines += '\nS4: '
    for w in W:
        lines += str(round(S4[w])) + ' '
    lines += '\nS5: '
    for w in W:
        lines += str(round(S5[w])) + ' '
    lines += '\nS6: {}'.format(round(S6))
    lines += '\nS7: {}\n'.format(round(S7))


        # f.writelines(lines)
    
    # for w in W:
    #     for n in N:
    #         for d in D:
    #             for s in S:
    #                 if sum(X[w][n, s, k, d] for k in K) > 0.9:
    #                     for k in K:
    #                         if X[w][n, s, k, d] > 0.9:
    #                             print(('w', w, 'n', n, 's', s, 'k', k, 'd', d, 'X', X[w][n, s, k, d]))

    return tot_cost