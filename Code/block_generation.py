import itertools
import time

from data import read_scenario, read_week, read_hist

def gen_master(W, N, S, K, skills, shift_names, shift_details, banned, work_blocks, \
            shift_blocks, break_blocks, contract_types, max_weekends, \
            complete_weekend, nurse_names, nurse_contracts, nurse_skills, orig_shift_blocks, orig_break_blocks):
            
    D = range(7)
    C = range(len(contract_types))

    # list of patterns that are defined as a sequence of work shifts, unconstrained
    # so far
    base_patterns = []
    for d in range(1, 8):
        new_blocks = set(itertools.product(range(len(S)), repeat=d))
        base_patterns += list(new_blocks)

    # set of feasible blocks
    feas_patterns = set()

    # set of feasible break stretches
    break_shift_blocks = [(len(S),) * d for d in D[1:]]

    # indicator used in defining successors
    bases = set()
    for block in base_patterns:
        found = False
        for (i, j) in list(zip(range(len(block)), range(1, len(block)))):
            if (block[i], block[j]) in banned:
                found = True
                break
        if not found:
            bases.add(block)
            feas_patterns.add(block)

    for b in break_shift_blocks:
        feas_patterns.add(b)
    feas_patterns = list(feas_patterns)

    # defines the list of blocks that can follow each block i
    successors = {i: [] for i in feas_patterns}
    predecessors = {i: [] for i in feas_patterns}

    #number of assignments for a particular block
    num_assignments = {b: sum((1 if b[i] != len(S) else 0) for i in range(len(b))) for b in feas_patterns}

    #calculate cost, shift positions and successors for each block
    start = time.time()
    base_cost = {b: 0 for b in feas_patterns}
    contract_cost = {(b, c): 0 for b in feas_patterns for c in C}
    shift_pos = {(b, s, p): 0 for b in feas_patterns for s in range(len(S) + 1) for p in range(len(b))}
    work_cost = {b: 0 for b in feas_patterns}
    for block in feas_patterns:
        for i in range(len(block)):
            shift_pos[block, block[i], i] = 1
            if (i == 0 or block[i - 1] != block[i]) and block[i] != len(S):
                done = False
                count = 1
                while not done:
                    if i + count < len(block) and block[i + count] == block[i]:
                        count += 1
                    else:
                        done = True
                cost =  15 * (max(shift_details[block[i]][0] - count, 0) + \
                                        max(count - shift_details[block[i]][1], 0))
                base_cost[block] += cost
 
        last_block = block[-count:]
        
        if block[0] == len(S):
            successors[block] = [i for i in feas_patterns if i[0] != len(S) and len(block) + len(i) <= 7]
            for c in C:
                contract_cost[block, c] += 30 * (max(orig_break_blocks[c][0] - len(block), 0) + \
                                        max(len(block) - orig_break_blocks[c][1], 0))
        else:
            successors[block] = [i for i in feas_patterns if i[0] == len(S) and len(block) + len(i) <= 7]
            for c in C:
                contract_cost[block, c] += 30 * (max(orig_shift_blocks[c][0] - len(block), 0) + \
                                                max(len(block) - orig_shift_blocks[c][1], 0))
                work_cost[block] += 30 * (max(orig_shift_blocks[c][0] - len(block), 0) + \
                                                max(len(block) - orig_shift_blocks[c][1], 0))
                
        for c in C:
            contract_cost[block, c] += base_cost[block]
        work_cost[block] += base_cost[block]

    end = time.time()

    return feas_patterns, contract_cost, num_assignments, shift_pos, successors, work_cost

def block_carry_costs(blocks, SW, N, shift_details, shift_blocks, break_blocks, \
    shift_hist, shift_len, work_len, break_len):

    D = range(7)
    overflow_costs = {(i, n): 0 for i in blocks for n in N}
    end_costs = {(i, n): 0 for i in blocks for n in N}

    # Define overflow costs- cost of placing a block at day 0 given history of previous week
    for (i, n) in overflow_costs:
        for p in range(len(i)):
            if i[p] != len(SW) and (p == 0 or i[p - 1] != i[p]):
                shift_done = False
                count_shift = 1
                while not shift_done:
                    if p + count_shift < len(i) and i[p + count_shift] == i[p]:
                        count_shift += 1
                    else:
                        shift_done = True
                if p == 0 and i[0] == shift_hist[n]:
                    overflow_costs[i, n] += 15 * (max(shift_details[i[0]][0] - count_shift - shift_len[n], 0) + \
                                                    max(count_shift + shift_len[n] - shift_details[i[0]][1], 0))
                elif p == 0 and i[0] != shift_hist[n] and shift_hist[n] != len(SW):
                    overflow_costs[i, n] += 15 * (max(shift_details[i[0]][0] - count_shift, 0) + \
                                                    max(count_shift - shift_details[i[0]][1], 0)) + \
                                            15 * max(shift_details[shift_hist[n]][0] - shift_len[n], 0)
                elif p != 0 or shift_hist[n] == len(SW):
                    if len(i) < len(D) and p + count_shift < len(D):
                        overflow_costs[i, n] += 15 * (max(shift_details[i[p]][0] - count_shift, 0) + \
                                                        max(count_shift - shift_details[i[p]][1], 0))
                    else:
                        overflow_costs[i, n] += 15 * max(count_shift - shift_details[i[p]][1], 0)
        if i[0] != len(SW):
            if work_len[n] > 0:
                overflow_costs[i, n] += 30 * (max(shift_blocks[n][0] - len(i) - work_len[n], 0) + \
                                                max(len(i) + work_len[n] - shift_blocks[n][1], 0))
            else:
                overflow_costs[i, n] += 30 * max(break_blocks[n][0] - break_len[n], 0) + \
                                        30 * (max(shift_blocks[n][0] - len(i), 0) + \
                                                max(len(i) - shift_blocks[n][1], 0))
        else:
            if break_len[n] > 0:
                overflow_costs[i, n] += 30 * (max(break_blocks[n][0] - len(i) - break_len[n], 0) + \
                                                max(len(i) + break_len[n] - break_blocks[n][1], 0))
            else:
                overflow_costs[i, n] += 30 * (max(shift_blocks[n][0] - work_len[n], 0)) + \
                                        30 * (max(break_blocks[n][0] - len(i), 0) + \
                                                max(len(i) - break_blocks[n][1], 0)) + \
                                        15 * max(shift_details[shift_hist[n]][0] - shift_len[n], 0) 

    for (i, n) in end_costs:
        if i[-1] != len(SW):
            for p in range(len(i)):
                if p == 0 or i[p - 1] != i[p]:
                    done = False
                    count = 1
                    while not done:
                        if p + count < len(i) and i[p + count] == i[p]:
                            count += 1
                        else:
                            done = True
                    if p + count < len(i):
                        end_costs[i, n] += 15 * (max(shift_details[i[p]][0] - count, 0) + \
                                                max(count - shift_details[i[p]][1], 0))
                    else:
                        end_costs[i, n] += 15 * max(count - shift_details[i[p]][1], 0)
            if len(i) > shift_blocks[n][1]:
                end_costs[i, n] += 30 * (len(i) - shift_blocks[n][1])
        elif i[-1] == len(SW) and len(i) > break_blocks[n][1]:
            end_costs[i, n] += 30 * (len(i) - break_blocks[n][1])

    return overflow_costs, end_costs

