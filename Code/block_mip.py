from gurobipy import *
import os
import shutil
import time
import math

from data import read_scenario, read_week, read_hist, write_hist, DATAPATH, BENCHMARK_TIMES
from block_generation import gen_master, block_carry_costs
from evaluate import eval_roster, eval_roster_file
import numpy as np


def solve_block_week(path, scen, week, blocks, block_costs, shift_pos, \
    successors, week_nums, W, N, S, K, skills, shift_names, shift_details, \
    banned, work_blocks, shift_blocks, break_blocks, max_weekends, \
    complete_weekend, nurse_names, nurse_contracts, nurse_skills, \
    mode = '', w6 = 7.8841, w7 = 14.3991, w8 = 19.9534, w6q = 0.3321, seed=-1):
                    
    """
    Solve the block formulation MIP implementation for a given week in the planning horizon

    Parameters
    ----------
    path : str
        Path of the folder containing relevant data

    scen : str
        Name of the scenario instance

    week : int
        Week number in planning horizon

    blocks : list
        List of feasible blocks

    block_cost : dict
        Dictionary mapping each nurse/block pair to the cost of assigning that nurse to that block

    shift_pos : dict
        Dictionary mapping (block, shift, pos) to 1 if block has shift at index pos, else 0
    
    successors : dict
        Dictionary mapping each block to list of valid successors

    week_nums : list
        List of week data file mumbers used in scenario instance

    W : list
        List of weeks

    N : list
        List of nurses

    S : list
        List of shifts

    K : list
        List of skills

    skills : list
        List of string names of skills

    shift_names : list
        List of string names of shifts

    banned : list
        List of tuples of banned shift sequence pairs

    work_blocks : list
        List of maximum numbers of assignments for each nurse

    shift_blocks : list
        List of min/max number of consecutive work assignments for each nurse

    break_blocks : list
        List of min/max number of consecutive breaks for each nurse

    max_weekends : list
        List of maximum numbers of weekends that can be worked by each nurse in horizon

    nurse_names : list
        List of string names of each nurse

    nurse_skills : dict
        Dictionary mapping each nurse to list of their skill types

    mode : str
        String that indicates whether we solve this problem using the history for the block formulation or the init MIP

    w6 : int
        Cost coefficient for linear term of CS62 in the objective 

    w7 : int
        Cost coefficient for CS72

    w8 : Cost coefficient for CS8

    seed : int
        Random seed value passed to Gurobi, default -1 for no specification

    Returns
    -------
    XV, YV, CS1V, CS4V, CS5V, CS6V, CS7V, CS62V, CS72V, CS8V: dictionaries of solved values for all variables

    m: model

    overflow_costs, end_costs- Cost data relating to overflow/end of week, returned for convenience later

    coverage, requests: week data returned for convenience later
    """
    D = range(7)


    # load data for the week
    coverage, requests = read_week(scen, path, 'WD-{}-{}'.format(scen, \
        week_nums[week]), S, K, N, shift_names, skills, nurse_names)

    # read the previous week's history (week zero reads the initial history file)
    if mode == 'Base':
        hist_name = 'H{}-{}Base'.format(week, scen)
    else:
        hist_name = 'H{}-{}Block'.format(week, scen)
    tot_ass, tot_weekend, shift_hist, shift_len, work_len, break_len = read_hist(path, hist_name, S, N, shift_names)

    # Set of working shifts
    SW = S
    # Set of all shifts
    S = range(len(SW) + 1)

    overflow_costs, end_costs = block_carry_costs(blocks, SW, N, shift_details, 
        shift_blocks, break_blocks, shift_hist, shift_len, work_len, break_len)

    has_skill = {(n, k): (1 if k in nurse_skills[n] else 0) for n in N for k in K}

    m = Model('INRC Block')
    m.setParam("TimeLimit", 900)
    m.setParam('LogFile', os.path.join(path, 'LogFileBlock.txt'))
    m.setParam('Method', 2)
    m.setParam('OutputFlag', 0)
    if seed > 0 and seed < 2000000000:
        m.setParam('Seed', seed)


    #variables
    # X[n, s, k, d] = 1 if nurse n works shift s with skill k on day d, 0 otherwise
    X = {(n, s, k, d): m.addVar(vtype=GRB.BINARY) 
            for n in N for s in S for k in K for d in D}
    
    # Y[n] = 1 if nurse n words on the weekend
    Y = {n: m.addVar(vtype=GRB.BINARY) for n in N}

    # U[n, i, d] = 1 if we place block i for nurse n starting on day d, 0 otherwise
    U = {(n, i, d): m.addVar(vtype = GRB.BINARY) 
            for n in N for i in blocks for d in D if len(i) + d <= 7}

    # variables relating to soft constraints
    # number of missing nurses for optimal coverage of shift s, skill k, day d 
    CS1 = {(s, k, d): m.addVar(vtype=GRB.INTEGER) for s in SW for k in K for d in D}

    # 1 if assignment on day d violates request for nurse n, 0 otherwise
    CS4 = {(n, d): m.addVar(vtype=GRB.BINARY) for n in N for d in D}

    # 1 if nurse n violates complete weekend constraint, 0 otherwise
    CS5 = {n: m.addVar(vtype=GRB.BINARY) for n in N}

    # number of total shifts outside the allowed bounds for nurse n
    CS6 = {n: m.addVar(vtype=GRB.INTEGER) for n in N}
    # number of total shifts outside the allowed bounds for average assignments 
    # for nurse n
    CS62 = {n: m.addVar(vtype = GRB.INTEGER) for n in N}

    # number of weekends worked outside of the allowed bounds for nurse n
    CS7 = {n: m.addVar(vtype=GRB.INTEGER) for n in N}
    # number of weekends worked outside of the allowed average bounds for nurse n
    CS72 = {n: m.addVar(vtype=GRB.INTEGER) for n in N}
    
    # Number of nurses assigned above optimal level
    CS8 = {(s, k, d): m.addVar(vtype = GRB.INTEGER) for s in SW for k in K for d in D}

    last_blocks = [(i, d) for i in blocks for d in D if d + len(i) == len(D)]
    mid_blocks = [(i, d) for i in blocks for d in D if d > 0 and d + len(i) < len(D)]

    # if week != W[-1]:
        # CS11 = {(s, k): m.addVar(vtype=GRB.BINARY) for s in SW for k in K}
        # CS11 = {(s, k): m.addVar(vtype=GRB.INTEGER) for s in SW for k in K}

    m.setObjective((quicksum(block_costs[n, i] * U[n, i, d] 
                        for n in N for (i, d) in mid_blocks) + \
                quicksum(end_costs[i, n] * U[n, i, d] for n in N for (i, d) in last_blocks) + \
                quicksum(overflow_costs[i, n] * U[n, i, 0] for (i, n) in overflow_costs if (n, i, 0) in U) + \
                30 * quicksum(CS1[s, k, d] for (s, k, d) in CS1) + \
                10 * quicksum(CS4[n, d] for (n, d) in CS4) + \
                30 * quicksum(CS5[n] for n in N) +\
                20 * quicksum(CS6[n] for n in N) + \
                30 * quicksum(CS7[n] for n in N)) + \
                w8 * quicksum(CS8[s, k, d] 
                        for (s, k, d) in CS8) + \
                # 9.9 * quicksum(CS62[n] for n in N) + \
                # 0.1 * quicksum(CS62[n] * CS62[n] for n in N) + \
                w7 * quicksum(CS72[n] for n in N), # + \
                # w11 * quicksum(CS11[s, k] for (s, k) in CS11),
                GRB.MINIMIZE)

    # formulate quadratic terms in objective as piecewise linear function
    xs, ys = pwslinear(w6, 25)
    for n in N:
        m.setPWLObj(CS62[n], xs, ys)

    # Block placement/flow constraints

    OverlapBlocks = {(n, s, d): [(i, p) for i in blocks for p in range(len(i)) 
            if i[p] == s and (n, i, d - p) in U]
    for n in N for s in S for d in D}

    BlocksToDays = {(n, s, d): m.addConstr(quicksum(U[n, i, d - p] * shift_pos[i, s, p]
            for (i, p) in OverlapBlocks[n, s, d]) == 
            quicksum(X[n, s, k, d] for k in K))
    for (n, s, d) in OverlapBlocks}

    OFConflicts = {n: [i for i in blocks if (shift_hist[n], i[0]) in banned] for n in N}
    
    BlockSuccessionOF = {n: m.addConstr(quicksum(U[n, i, 0] for i in OFConflicts[n]) == 0) for n in N}

    work_set = [i for i in blocks if i[0] != len(SW)]
    break_set = [i for i in blocks if i[0] == len(SW)]

    valid_work_pred = {d: [i for i in work_set if d - len(i) >= 0] for d in D[1:]}
    valid_break_succ = {d: [i for i in break_set if d + len(i) <= 7] for d in D}

    valid_work_succ = {d: [i for i in work_set if d + len(i) <= 7] for d in D}
    valid_break_pred = {d: [i for i in break_set if d - len(i) >= 0] for d in D[1:]}

    BlockWorkFlow = {(n, d): m.addConstr(
        quicksum(U[n, i, d - len(i)] for i in valid_work_pred[d]) <= 
        quicksum(U[n, j, d] for j in valid_break_succ[d]))
    for n in N for d in D[1:]}

    BlockBreakFlow = {(n, d): m.addConstr(
        quicksum(U[n, i, d - len(i)] for i in valid_break_pred[d]) <= 
        quicksum(U[n, j, d] for j in valid_work_succ[d]))
    for n in N for d in D[1:]}

    # Hard constraints

    OneAssignmentPerDay = {(n, d): m.addConstr(quicksum(X[n, s, k, d] 
            for s in S for k in K) == 1)
    for n in N for d in D}
    
    MinCover = {(s, k, d): m.addConstr(
            quicksum(X[n, s, k, d] for n in N) >= coverage[s, k, d][0])
    for s in SW for k in K for d in D}

    HasSkill = {(n, s, k, d): m.addConstr(
            X[n, s, k, d] == 0)
    for (n, s, k, d) in X if k not in nurse_skills[n]}

    # Soft constraints

    # S1
    SubOPtimal = {(s, k, d): m.addConstr(
            quicksum(X[n, s, k, d] for n in N) >= \
            coverage[s, k, d][1] - CS1[s, k, d])
    for (s, k, d) in CS1}

    # S4
    ShiftRequests = {(n, s, d): m.addConstr(
                quicksum(X[n, s, k, d] for k in K) <= CS4[n, d])
    for n in N for s in SW for d in D if requests[n, s, d] == 1}

    # S5
    Weekend1 = {(n, d): m.addConstr(quicksum(X[n, s, k, d] 
                for s in SW for k in K) <= Y[n])
    for n in N for d in [5, 6]}

    Weekend2 = {n: m.addConstr(quicksum(X[n, s, k, 5] + X[n, s, k, 6] 
                for s in SW for k in K) >=
                2 * Y[n] - CS5[n])
    for n in N if complete_weekend[n] == 1}

    # S6
    TotAssignmentsU = {n: m.addConstr(quicksum(X[n, s, k, d] 
                for s in SW for k in K for d in D) <= 
                max(work_blocks[n][1] - tot_ass[n], 0) + CS6[n])
    for n in N}

    TotAssignmentsL = {n: m.addConstr(quicksum(X[n, s, k, d] 
                    for s in SW for k in K for d in D) >= 
                    min(work_blocks[n][0] - \
                    len(D) * (len(W) - week), len(D)) - CS6[n])
    for n in N}

    # S7
    TotWeekends = {n: m.addConstr(min(tot_weekend[n], max_weekends[n]) + Y[n] <= 
                    max_weekends[n] + CS7[n])
    for n in N}

    # Extensions

    # S8*
    OverStaffing = {(s, k, d): m.addConstr(quicksum(X[n, s, k, d] for n in N) <= 
                    coverage[s, k, d][1] + CS8[s, k, d])
    for s in SW for k in K for d in D}

    # S6*
    AvAssU = {n: m.addConstr(tot_ass[n] + quicksum(X[n, s, k, d] 
                for s in SW for k in K for d in D) <= 
                math.floor(work_blocks[n][1] * (week + 1)/len(W)) + CS62[n])
    for n in N}

    AvAssL = {n: m.addConstr(tot_ass[n] + quicksum(X[n, s, k, d] 
                for s in SW for k in K for d in D) >= 
                math.ceil(work_blocks[n][0] * (week + 1)/len(W)) - CS62[n])
    for n in N}

    AvRemAssU = {n: m.addConstr(quicksum(X[n, s, k, d] 
                    for s in SW for k in K for d in D) <= 
                    math.floor((work_blocks[n][1] - tot_ass[n]) * 1/(len(W) - week)) + CS62[n])
    for n in N}

    AvRemAssL = {n: m.addConstr(quicksum(X[n, s, k, d] 
                    for s in SW for k in K for d in D) >= 
                    math.ceil((work_blocks[n][0] - tot_ass[n]) * \
                    1/(len(W) - week)) - CS62[n])
    for n in N}


    # S7*
    AvWeekends = {n: m.addConstr(Y[n] <= 
                    math.floor((max(max_weekends[n] - tot_weekend[n], 0)) * \
                    1/(len(W) - week)) + CS72[n]) 
    for n in N}

    m.optimize()

    # find and save history data for next week using current week's data
    last_shift = ['None'] * len(N)
    shift_stretch = [0] * len(N)
    work_stretch = [0] * len(N)
    break_stretch = [0] * len(N)

    for n in N:
        tot_weekend[n] += Y[n].x
        tot_ass[n] += sum(X[n, s, k, d].x for s in SW for k in K for d in D)
        found = 0
        for s in SW:
            if sum(X[n, s, k, 6].x for k in K) > 0.9:
                last_shift[n] = shift_names[s]
                found = 1
        
        done_work = False
        done_shift = False
        done_break = False
        
        if found:
            d = 0
            while not done_work:
                if d<= 6 and sum(X[n, s, k, len(D) - d - 1].x for s in SW for k in K) > 0.9:
                    work_stretch[n] += 1
                    d += 1
                else:
                    done_work = True
            d = 0
            while not done_shift:
                if d<= 6 and sum(X[n, shift_names.index(last_shift[n]), k, len(D) - d - 1].x for k in K) > 0.9:
                    shift_stretch[n] += 1
                    d += 1
                else:
                    done_shift = True
        else:
            d = 0
            while not done_break:
                if d <= 6 and sum(X[n, s, k, len(D) - d - 1].x for s in SW for k in K) < 0.1:
                    break_stretch[n] += 1
                    d += 1
                else:
                    done_break = True

    filename = 'H{}-{}Block.txt'.format(week + 1, scen)
    write_hist(path, filename, scen, week + 1, SW, N, K, nurse_names, shift_names, \
                tot_ass, tot_weekend, last_shift, shift_stretch, \
                work_stretch, break_stretch)
    # store the values of variables to use in the scenario solver

    XV = {(n, s, k, d): X[n, s, k, d].x for (n, s, k, d) in X}

    YV = {n: Y[n].x for n in Y}

    UV = {(n, i, d): U[n, i, d].x for (n, i, d) in U}

    CS1V = {(s, k, d): CS1[s, k, d].x for (s, k, d) in CS1}

    CS4V = {(n, d): CS4[n, d].x for (n, d) in CS4}

    CS5V = {n: CS5[n].x for n in CS5}

    CS6V = {n: CS6[n].x for n in CS6}
    CS62V = {n: CS62[n].x for n in CS62}

    CS7V = {n: CS7[n].x for n in CS7}
    CS72V = {n: CS72[n].x for n in CS72}

    CS8V = {(s, k, d): CS8[s, k, d].x for (s, k, d) in CS8}

    return XV, YV, UV, CS1V, CS4V, CS5V, CS6V, CS7V, CS62V, CS72V, CS8V, m, overflow_costs, end_costs, coverage, requests


def solve_block_scen(scen, hist_num, week_nums, w6 = 7.8841, w7 = 14.3991, w8 = 19.9534, w6q = 0.3321, seed=-1):
    """
    Solve a scenario instance using block formulation

    Parameters
    ----------
    scen : str
        Name of scenario instance

    hist_num : int
        Number label of history file used (appears at end of original file name)

    week_nums : list
        List of integer labels of week data files used in this instance

    w6 : int
        Cost coefficient for linear term of CS62 in the objective 

    w7 : int
        Cost coefficient for CS72

    w8 : Cost coefficient for CS8

    seed : int
        Random seed value passed to Gurobi, default -1 for no specification

    Returns
    -------
    XVS, YVS, CS1VS, CS4VS, CS5VS, CS6VS, CS62VS, CS7VS, CS72VS, CS8VS- dictionaries storing solved variables for each week of scenario instance
    """
    W, N, S, K, skills, shift_names, shift_details, banned, work_blocks, \
        shift_blocks, break_blocks, contract_types, max_weekends, \
        complete_weekend, nurse_names, nurse_contracts, nurse_skills, \
        orig_shift_blocks, orig_break_blocks = read_scenario(scen, 'Sc-{}'.format(scen))

    #generate set of feasible blocks and related cost and succession info
    blocks, contract_costs, assignments, shift_pos, successors, work_cost = gen_master(W, \
        N, S, K, skills, shift_names, shift_details, banned, work_blocks, shift_blocks, \
        break_blocks, contract_types, max_weekends, complete_weekend, nurse_names, \
        nurse_contracts, nurse_skills, orig_shift_blocks, orig_break_blocks)

    # change block costs to be indexed by nurse rather than contract type
    block_costs = {(n, i): contract_costs[i, contract_types.index(nurse_contracts[n])]
                    for n in N for i in blocks}

    # folder we store data and results in 
    sol_folder = 'Solution to H_{}-WD_{}'.format(hist_num, week_nums[0])
    for i in range(1, len(week_nums)):
        sol_folder += '-{}'.format(week_nums[i])

    # check if paths correct/exist- if not, make new path
    orig_path = os.path.join(DATAPATH, scen)
    new_path = os.path.join(orig_path, 'Rep Sols', sol_folder)

    if not os.path.isdir(orig_path):
        os.makedirs(orig_path)

    try:
        open(os.path.join(orig_path, 'Sc-{}.txt'.format(scen)))
    except FileNotFoundError:
        orig_path = DATAPATH
    
    if not os.path.isdir(os.path.join(orig_path, 'Rep Sols')):
        os.makedirs(os.path.join(orig_path, 'Rep Sols'))
    if not os.path.isdir(new_path):
        os.makedirs(new_path)

    # copy scenario data into local directory
    shutil.copy(os.path.join(orig_path, 'Sc-{}.txt'.format(scen)), \
                os.path.join(new_path, 'Sc-{}.txt'.format(scen)))
    # copy week data into local directory
    for w in week_nums:
        shutil.copy(os.path.join(orig_path, 'WD-{}-{}.txt'.format(scen, w)), \
                    os.path.join(new_path, 'WD-{}-{}.txt'.format(scen, w)))
    # copy history file into local directory
    shutil.copy(os.path.join(orig_path, 'H0-{}-{}.txt'.format(scen, hist_num)), \
                os.path.join(new_path, 'H0-{}Block.txt'.format(scen)))

    if os.path.isfile(os.path.join(new_path, 'LogFileBlock.txt')):
        os.remove(os.path.join(new_path, 'LogFileBlock.txt'))


    XVS = {}
    YVS = {}
    UVS = {}
    CS1VS = {}
    CS4VS = {}
    CS5VS = {}
    CS6VS = {}
    CS62VS = {}
    CS7VS = {}
    CS72VS = {}
    CS8VS = {}
    MS = {}
    OF = {}
    EC = {}
    coverage = {}
    requests = {}
    timeout = [0] * len(W)
    num_weeks = len(W)
    D = range(7)
    for w in W:
        start = time.time()
        XV, YV, UV, CS1V, CS4V, CS5V, CS6V, CS7V, CS62V, CS72V, CS8V, m, overflow_costs, end_costs, cov_dat, req_dat = solve_block_week(new_path, \
            scen, w, blocks, block_costs,
            shift_pos, successors, week_nums, W, N, S, K, skills, shift_names, \
            shift_details, banned, work_blocks, shift_blocks, break_blocks, max_weekends, \
            complete_weekend, nurse_names, nurse_contracts, nurse_skills, w6=w6, w7=w7, w8=w8, w6q=w6q, seed=seed)
        end = time.time()
        XVS[w] = XV
        YVS[w] = YV
        UVS[w] = UV
        CS1VS[w] = CS1V
        CS4VS[w] = CS4V
        CS5VS[w] = CS5V
        CS6VS[w] = CS6V
        CS7VS[w] = CS7V
        CS62VS[w] = CS62V
        CS72VS[w] = CS72V
        CS8VS[w] = CS8V
        MS[w] = m
        OF[w] = overflow_costs
        EC[w] = end_costs
        coverage[w] = cov_dat
        requests[w] = req_dat
        if m.Runtime >= 2500:
            timeout[w] = 1
    day_names = ['M', 'T', 'W', 'T', 'F', 'S', 'S']
    D = range(7)
    shift_letters = [shift_names[s][0] for s in S] + ['-']

    # write results to results file
    lines = ''

    num_ass = sum(XVS[w][n, s, k, d] for w in W[:num_weeks] for (n, s, k, d) in XVS[w] if s in S)
    avRunTime = sum(MS[w].Runtime for w in W[:num_weeks])/len(W[:num_weeks])
    lines += 'ASSIGNMENTS = {}\n'.format(round(num_ass))
    lines += 'TOTAL COST: {}\n'.format(round(sum(MS[w].objVal for w in W[:num_weeks])))
    lines += 'AVERAGE RUNTIME: {}\n\n'.format(round(avRunTime, 2))

    lines += '\t'
    for w in W[:num_weeks]:
        lines += '|'
        for d in D:
            lines += '{}|'.format(day_names[d])
        lines += '  '
    lines += '\n' + '-' * 74 + '\n'

    for n in N:
        lines += nurse_names[n] + '\t'
        for w in W[:num_weeks]:
            lines += '|'
            for d in D:
                found = False
                for s in range(len(S) + 1):
                    for k in nurse_skills[n]:
                        if XVS[w][n, s, k, d] > 0.9:
                            found = True
                            lines += '{}|'.format(shift_letters[s])
                if not found:
                    lines += '*|'
            lines += '  '
        lines += '\n'
    lines += '-' * 74
    lines += '\n\nWEEKLY RESULTS\n'
    lines += '-' * 24 + '\n'

    for w in W[:num_weeks]:
        lines += 'Week {}\n'.format(w)
        
        # calculate roster costs for each week based on soft constraint violations
        opt_cost = sum(30 * CS1VS[w][s, k, d] for (s, k, d) in CS1VS[w])
        req_cost = sum(10 * CS4VS[w][n, d] for n in N for d in D)
        comp_cost = sum(30 * CS5VS[w][n] for n in N)
        tot_ass_cost = sum(20 * CS6VS[w][n] for n in N)
        tot_weekend_cost = sum(30 * CS7VS[w][n] for n in N)
        if w != W[-1]:
            over_opt_cost = sum(11.9 * CS8VS[w][s, k, d] for (s, k, d) in CS8VS[w])
            av_ass_cost = sum(9.9 * CS62VS[w][n] for n in N)
            av_weekend_cost = sum(9.9 * CS72VS[w][n] for n in N)
        else:
            over_opt_cost = 0
            av_ass_cost = 0
            av_weekend_cost = 0

        hist_cost = sum(OF[w][i, n] * UVS[w][n, i, 0] for (i, n) in OF[w])
        

        lines += '-' * 24 + '\n'
        lines += 'OBJECTIVE VALUE: {}\n'.format(round(MS[w].objVal))
        lines += 'RUNTIME: {}\n'.format(round(MS[w].Runtime, 2))
        lines += '-' * 24 + '\n'
        
        blocks_cost = sum(block_costs[n, i] * UVS[w][n, i, d] for (n, i, d) in UVS[w] if d > 0 and d + len(i) < 7)
        end_cost = sum(end_costs[i, n] * UVS[w][n, i, d] for (i, n) in end_costs for d in D if d + len(i) == 7)
        lines += 'Block Cost: {}\n'.format(round(blocks_cost))
        lines += 'End Cost: {}\n'.format(round(end_cost))

        lines += 'Overflow Cost: {}\n'.format(round(hist_cost))
        lines += 'Below Optimality Cost: {}\n'.format(round(opt_cost))
        lines += 'Requests Cost: {}\n'.format(round(req_cost))
        lines += 'Complete Weekend Cost: {}\n'.format(round(comp_cost))
        lines += 'Total Assignments Cost: {}\n'.format(round(tot_ass_cost))
        lines += 'Total Weekends Cost: {}\n'.format(round(tot_weekend_cost))
        lines += 'Average Assignments: {}\n'.format(round(av_ass_cost))
        lines += 'Average Weekends Worked: {}\n'.format(round(av_weekend_cost))
        lines += 'Over Optimal Coverage: {}\n'.format(round(over_opt_cost))
        lines += '\n\n'
    
    with open(os.path.join(new_path, 'ResultsBlock.txt'), 'w') as f:
        f.writelines(lines)

    # evaluate the roster quality and save results to file
    tot_cost = eval_roster(scen, hist_num, week_nums, new_path, 'ResultsBlock.txt', 'Block', XVS, YVS, N, range(len(S) + 1), K, W, D, \
        coverage, banned, nurse_skills, shift_details, shift_names, shift_blocks, break_blocks, \
        requests, complete_weekend, work_blocks, max_weekends)

    
    
    return tot_cost, round(avRunTime, 2), timeout

def pwslinear(w6, xmax):
    """
    Define a piecewise linear function that is equuivalent to a quadratic objective

    Parameters
    ----------
    w6 : int
        Cost coefficient of the linear part of CS62
    xmax : int
        Maximum x-value that we evaluate the function to. After this the function will be a constant linear expression

    Returns
    -------
    xs
        List of x-values of points in the piecewise linear objective
    ys
        List of y-values of points in the piecewise linear objective
    """    
    xs = range(xmax + 1)
    ys = [0] * (xmax + 1)
    for x in xs:
        ys[x] = w6 + 0.1 * x**2
    return xs, ys

