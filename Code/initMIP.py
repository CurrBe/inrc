from gurobipy import *
import os
import shutil
import matplotlib.pyplot as plt
import time
import math


from data import read_scenario, read_week, read_hist, write_hist, DATAPATH, BENCHMARK_TIMES
from evaluate import eval_roster

def solve_week(path, scen, week, week_nums, W, N, S, K, skills, shift_names, \
    shift_details, banned, work_blocks, shift_blocks, break_blocks, max_weekends, \
    complete_weekend, nurse_names, nurse_contracts, nurse_skills):
    """
    Solve the initial MIP implementation for a given week in the planning horizon

    Parameters
    ----------
    path : str
        Path of the folder containing relevant data

    scen : str
        Name of the scenario instance

    week : int
        Week number in planning horizon

    week_nums : list
        List of week data file mumbers used in scenario instance

    W : list
        List of weeks

    N : list
        List of nurses

    S : list
        List of shifts

    K : list
        List of skills

    skills : list
        List of string names of skills

    shift_names : list
        List of string names of shifts

    banned : List
        List of tuples of banned shift sequence pairs

    work_blocks : list
        List of maximum numbers of assignments for each nurse

    shift_blocks : list
        List of min/max number of consecutive work assignments for each nurse

    break_blocks : list
        List of min/max number of consecutive breaks for each nurse

    max_weekends : list
        List of maximum numbers of weekends that can be worked by each nurse in horizon

    nurse_names : list
        List of string names of each nurse

    nurse_skills : dict
        Dictionary mapping each nurse to list of their skill types

    Returns
    -------
    XV, YV, CS1V, CS2AV, CS2BV, CS2CV, CS2DV, CS3AV, CS3BV, CS4V, CS5V, CS6V, CS7V, CS62V, CS72V, CS8V: dictionaries of solved values for all variables
    m: model

    coverage, requests: week data returned for convenience later
    """

    # check if desired path exists
    if not os.path.isdir(os.path.join(DATAPATH, scen, 'Rep Sols', )):
        raise ValueError('No valid scenario data at {}'.format(path))
    
    # load data for the week
    coverage, requests = read_week(scen, path, 'WD-{}-{}'.format(scen, week_nums[week]), S, K, N, shift_names, skills, nurse_names)

    # set of days
    D = range(7)

    # read the previous week's history (week zero reads the initial history file)
    hist_name = 'H{}-{}Base'.format(week, scen)
    tot_ass, tot_weekend, shift_hist, shift_len, work_len, break_len = read_hist(path, hist_name, S, N, shift_names)

    # model
    m = Model('INRC')
    m.setParam("TimeLimit", 3600)
    m.setParam('LogFile', os.path.join(path, 'LogFileBase.txt'))

    # variables

    # X[n, s, k, d] = 1 if nurse n works shift s with skill k on day d, 0 otherwise
    X = {(n, s, k, d): m.addVar(vtype=GRB.BINARY) 
            for n in N for s in S for k in K for d in D}
    
    # Y[n] = 1 if nurse n words on the weekend
    Y = {n: m.addVar(vtype=GRB.BINARY) for n in N}
    
    # variables relating to soft constraints

    # number of missing nurses for optimal coverage of shift s, skill k, day d 
    CS1 = {(s, k, d): m.addVar(vtype=GRB.INTEGER) for s in S for k in K for d in D}

    # missing days in block of shifts s for nurse n starting day d
    CS2A = {(n, s, d): m.addVar(vtype=GRB.INTEGER) for n in N for s in S for d in D}
    # 1 if shift s of nurse n on day d violates maximum consecutive shifts, 0 otherwise
    CS2B = {(n, s, d): m.addVar(vtype=GRB.BINARY) for n in N for s in S for d in D}
    
    # missing days in work block for nurse n starting day d
    CS2C = {(n, d): m.addVar(vtype=GRB.INTEGER) for n in N for d in D}
    # 1 if work of nurse n on day d violates maximum consecutive shifts, 0 otherwise
    CS2D = {(n, d): m.addVar(vtype=GRB.BINARY) for n in N for d in D}
    
    # missing days in break block for nurse n starting day d
    CS3A = {(n, d): m.addVar(vtype=GRB.INTEGER) for n in N for d in D}
    # 1 if work of nurse n on day d violates maximum consecutive shifts, 0 otherwise
    CS3B = {(n, d): m.addVar(vtype=GRB.BINARY) for n in N for d in D}

    # 1 if assignment on day d violates request for nurse n, 0 otherwise
    CS4 = {(n, d): m.addVar(vtype=GRB.BINARY) for n in N for d in D}

    # 1 if nurse n violates complete weekend constraint, 0 otherwise
    CS5 = {n: m.addVar(vtype=GRB.BINARY) for n in N}

    # number of total shifts outside the allowed bounds for nurse n
    CS6 = {n: m.addVar(vtype=GRB.INTEGER) for n in N}
    # number of total shifts outside the allowed bounds for average assignments 
    # for nurse n
    CS62 = {n: m.addVar(vtype = GRB.INTEGER) for n in N}

    # number of weekends worked outside of the allowed bounds for nurse n
    CS7 = {n: m.addVar(vtype=GRB.INTEGER) for n in N}
    # number of weekends worked outside of the allowed average bounds for nurse n
    CS72 = {n: m.addVar(vtype=GRB.INTEGER) for n in N}
    
    # Number of nurses assigned above optimal level
    CS8 = {(s, k, d): m.addVar(vtype = GRB.INTEGER) for s in S for k in K for d in D}

    # Objective
    m.setObjective(30 * quicksum(CS1[s, k, d] for (s, k, d) in CS1) + \
                    15 * quicksum(CS2A[n, s, d] + CS2B[n, s, d] 
                                    for n in N for s in S for d in D) + \
                    30 * quicksum(CS2C[n, d] + CS2D[n, d] 
                                    for n in N for d in D) + \
                    30 * quicksum(CS3A[n, d] + CS3B[n, d] 
                                    for n in N for d in D) + \
                    10 * quicksum(CS4[n, d] for n in N for d in D) + \
                    30 * quicksum(CS5[n] for n in N) + \
                    20 * quicksum(CS6[n] for n in N) + \
                    30 * quicksum(CS7[n] for n in N) + \
                    11.9 * quicksum(CS8[s, k, d] 
                                    for s in S for k in K for d in D) + \
                    9.9 * quicksum(CS62[n] for n in N) + \
                    9.9 * quicksum(CS72[n] for n in N), 
                    GRB.MINIMIZE)

    # hard constraints
    # H1
    MaxOneShift = {(n, d): m.addConstr(
            quicksum(X[n, s, k, d] for s in S for k in K) <= 1)
    for n in N for d in D}

    # H2
    MinCover = {(s, k, d): m.addConstr(
            quicksum(X[n, s, k, d] for n in N) >= coverage[s, k, d][0])
    for s in S for k in K for d in D}

    # H3A
    BannedSeq = {(n, b, d): m.addConstr(
            quicksum(X[n, b[0], k, d] for k in K) + \
            quicksum(X[n, b[1], k, d + 1] for k in K) <= 1)
    for n in N for b in banned for d in D[:-1]}

    
    # H3B
    BannedOF = {(n, s, k): m.addConstr(
            X[n, s, k, 0] == 0)
    for n in N for s in S for k in K if (shift_hist[n], s) in banned}

    # H4
    HasSkill = {(n, s, k, d): m.addConstr(
            X[n, s, k, d] == 0)
    for (n, s, k, d) in X if k not in nurse_skills[n]}

    
    # soft constraints
    # S1
    SubOPtimal = {(s, k, d): m.addConstr(
            quicksum(X[n, s, k, d] for n in N) >= \
            coverage[s, k, d][1] - CS1[s, k, d])
    for (s, k, d) in CS1}

    # S2A
    MissingShifts = {(s, n, b, d): m.addConstr(
            quicksum(X[n, s, k, d]  + X[n, s, k, d + b + 1] for k in K) + \
            b - quicksum(X[n, s, k, d + i] for i in range(1, b + 1) for k in K) >=
            1 - CS2A[n, s, d + 1]/(shift_details[s][0] - b))
    for s in S for n in N for b in range(1, shift_details[s][0]) 
    for d in range(7 - (b + 1))}

    # cost incurred if we continue a shift stretch from previous week at start of current week
    MissingShiftsOF = {(s, n, b): m.addConstr(
           b - quicksum(X[n, s, k, i] for k in K for i in range(b)) + \
                    quicksum(X[n, s, k, b] for k in K) >= 
            1 - CS2A[n, s, 0]/(shift_details[s][0] - shift_len[n] - b))
    for s in S for n in N for b in range(1, shift_details[s][0] - shift_len[n]) if shift_hist[n] == s}

    # cost incurred if we start a new shift stretch in the current week on day 0
    MissingShiftOF2 = {(s, n): m.addConstr(
            quicksum(X[n, s, k, 0] for k in K) >= 
            1 - CS2A[n, s, 0]/(shift_details[s][0] - shift_len[n]))
    for s in S for n in N if shift_hist[n] == s and shift_len[n] < shift_details[s][0]}

    # penalised if we are below minimum shift length at end of previous week and don't continue in current week
    MissingShiftsNew = {(s, n, b): m.addConstr(
        b - quicksum(X[n, s, k, i] for i in range(b) for k in K) + \
        quicksum(X[n, s, k, b] for k in K) >= 
        1 - CS2A[n, s, 0]/(shift_details[s][0] - b))
    for s in S for n in N for b in range(1, shift_details[s][0]) if s != shift_hist[n]}
    
    # S2b
    ExtraShifts = {(s, n, d): m.addConstr(
            quicksum(X[n, s, k, d + i] 
            for k in K for i in range(shift_details[s][1] + 1)) <= 
            shift_details[s][1] + CS2B[n, s, d + shift_details[s][1]])
    for s in S for n in N for d in range(7 - (shift_details[s][1]))}

    ExtraShiftsOF = {(s, n, b): m.addConstr(
            quicksum(X[n, s, k, i] for k in K for i in range(b)) <= 
            b - 1 + CS2B[n, s, b]) for s in S for n in N 
    for b in range(max(shift_details[s][1] - shift_len[n], 0) + 1, \
        min(len(D), shift_details[s][1] + 1)) if shift_hist[n] == s}

    # S2c
    MissingWork = {(n, b, d): m.addConstr(
            quicksum(X[n, s, k, d] + X[n, s, k, d + b + 1] for s in S for k in K) + \
            b - quicksum(X[n, s, k, d + i] for i in range(1, b + 1) for s in S for k in K) >= 
            1 - CS2C[n, d + 1]/(shift_blocks[n][0] - b))
    for n in N for b in range(1, shift_blocks[n][0]) for d in range(7 - (b + 1))}

    MissingWorkOF = {(n, b): m.addConstr(
            b - quicksum(X[n, s, k, i] for i in range(b) for s in S for k in K) + \
            quicksum(X[n, s, k, b] for s in S for k in K) >= 
            1 - CS2C[n, 0]/(shift_blocks[n][0] - work_len[n] - b))
    for n in N for b in range(1, shift_blocks[n][0] - work_len[n])}

    # penalty if under minimum work length at end of prev week and don't continue stretch in current week day 0
    MissingWorkOF2 = {n: m.addConstr(
                        quicksum(X[n, s, k, 0] for s in S for k in K) >= 
                        1 - CS2C[n, 0]/(shift_blocks[n][0] - work_len[n]))
    for n in N if shift_hist[n] != len(S) and work_len[n] < shift_blocks[n][0]}

    # S2d
    ExtraWork = {(n, d): m.addConstr(quicksum(X[n, s, k, d + i] 
                    for s in S for k in K 
                    for i in range(shift_blocks[n][1] + 1)) <= \
                shift_blocks[n][1] + CS2D[n, d + shift_blocks[n][1]])
    for n in N for d in range(7 - shift_blocks[n][1])}

    ExtraWorkOF = {(n, b): m.addConstr(
                    quicksum(X[n, s, k, i] 
                    for s in S for k in K for i in range(b)) <= 
                    b - 1 + CS2D[n, b - 1])
    for n in N for b in range(max(shift_blocks[n][1] - work_len[n], 0) + 1, \
        min(shift_blocks[n][1] + 1, len(D))) if shift_hist[n] != len(S)}

    # S3a
    MissingBreak = {(n, b, d): m.addConstr(1 - sum(X[n, s, k, d] for s in S for k in K) + \
                        sum(X[n, s, k, d + i] for i in range(1, b + 1) for s in S for k in K) + \
                        1 - sum(X[n, s, k, d + b + 1] for s in S for k in K) >= 
                1 - CS3A[n, d + 1]/(break_blocks[n][0] - b))
        for n in N for b in range(1, break_blocks[n][0])
        for d in range(7 - (b + 1))}

    MissingBreakOF = {(n, b): m.addConstr(
            quicksum(X[n, s, k, i] for i in range(b) for s in S for k in K) - \
            quicksum(X[n, s, k, b] for s in S for k in K) >= 
            -CS3A[n, 0]/(break_blocks[n][0] - break_len[n] - b))
    for n in N for b in range(1, break_blocks[n][0] - break_len[n])}

    # penalty if below minimum break length at end of previous week and don't continue in current week day 0
    MissingBreakOF2 = {n: m.addConstr(
            quicksum(-X[n, s, k, 0] for s in S for k in K) >= 
            -CS3A[n, 0]/(break_blocks[n][0] - break_len[n]))
    for n in N if shift_hist[n] == len(S) and break_len[n] < break_blocks[n][0]}

    # S3b
    ExtraBreak = {(n, d): m.addConstr(
                quicksum(X[n, s, k, d + i] for s in S for k in K 
                        for i in range(break_blocks[n][1] + 1)) >= 
                1 - CS3B[n, d + break_blocks[n][1]])
    for n in N for d in range(7 - break_blocks[n][1])}

    ExtraBreakOF = {(n, b): m.addConstr(
                quicksum(X[n, s, k, i] 
                    for s in S for k in K for i in range(b + 1)) >=
                1 - CS3B[n, b])
    for n in N for b in range(max(break_blocks[n][1] - break_len[n], 0), \
        min(break_blocks[n][1] + 1, len(D))) if shift_hist[n] == len(S)}

    # S4
    ShiftRequests = {(n, s, d): m.addConstr(
                quicksum(X[n, s, k, d] for k in K) <= CS4[n, d])
    for n in N for s in S for d in D if requests[n, s, d] == 1}

    # S5
    Weekend1 = {(n, d): m.addConstr(quicksum(X[n, s, k, d] 
                for s in S for k in K) <= Y[n])
    for n in N for d in [5, 6]}

    Weekend2 = {n: m.addConstr(quicksum(X[n, s, k, 5] + X[n, s, k, 6] 
                for s in S for k in K) >=
                2 * Y[n] - CS5[n])
    for n in N if complete_weekend[n] == 1}

    # S6
    TotAssignmentsU = {n: m.addConstr(quicksum(X[n, s, k, d] 
                for s in S for k in K for d in D) <= 
                max(work_blocks[n][1] - tot_ass[n], 0) + CS6[n])
    for n in N}

    TotAssignmentsL = {n: m.addConstr(quicksum(X[n, s, k, d] 
                    for s in S for k in K for d in D) >= 
                    min(work_blocks[n][0] - \
                    len(D) * (len(W) - week), len(D)) - CS6[n])
    for n in N}

    # S7
    TotWeekends = {n: m.addConstr(min(tot_weekend[n], max_weekends[n]) + Y[n] <= 
                    max_weekends[n] + CS7[n])
    for n in N}

    # Extensions

    # S8*
    OverStaffing = {(s, k, d): m.addConstr(quicksum(X[n, s, k, d] for n in N) <= 
                    coverage[s, k, d][1] + CS8[s, k, d])
    for s in S for k in K for d in D}
    
    # S6*
    AvAssU = {n: m.addConstr(tot_ass[n] + quicksum(X[n, s, k, d] 
                for s in S for k in K for d in D) <= 
                math.floor(work_blocks[n][1] * (week + 1)/len(W)) + CS62[n])
    for n in N}

    AvAssL = {n: m.addConstr(tot_ass[n] + quicksum(X[n, s, k, d] 
                for s in S for k in K for d in D) >= 
                math.ceil(work_blocks[n][0] * (week + 1)/len(W)) - CS62[n])
    for n in N}

    AvRemAssU = {n: m.addConstr(quicksum(X[n, s, k, d] 
                    for s in S for k in K for d in D) <= 
                    math.floor((work_blocks[n][1] - tot_ass[n]) * \
                    1/(len(W) - week)) + CS62[n])
    for n in N}

    AvRemAssL = {n: m.addConstr(quicksum(X[n, s, k, d] 
                    for s in S for k in K for d in D) >= 
                    math.ceil((work_blocks[n][0] - tot_ass[n]) * \
                    1/(len(W) - week)) - CS62[n])
    for n in N}

    # S7*
    AvWeekends = {n: m.addConstr(Y[n] <= 
                    math.floor((max_weekends[n] - tot_weekend[n]) * \
                    1/(len(W) - week)) + CS72[n]) 
    for n in N}

    m.optimize()

    # find and save history data for next week using current week's data
    last_shift = ['None'] * len(N)
    shift_stretch = [0] * len(N)
    work_stretch = [0] * len(N)
    break_stretch = [0] * len(N)

    for n in N:
        tot_weekend[n] += Y[n].x
        tot_ass[n] += sum(X[n, s, k, d].x for s in S for k in K for d in D)
        found = 0
        for s in S:
            if sum(X[n, s, k, 6].x for k in K) > 0.9:
                last_shift[n] = shift_names[s]
                found = 1
        
        done_work = False
        done_shift = False
        done_break = False
        d = 0

        if found:
            while not done_work or not done_shift:
                if sum(X[n, s, k, len(D) - d - 1].x 
                        for s in S for k in K) > 0.9 and not done_work:
                    work_stretch[n] += 1
                else:
                    done_work = True
                if sum(X[n, shift_names.index(last_shift[n]), k, len(D) - d - 1].x 
                        for k in K) > 0.9:
                    shift_stretch[n] += 1
                else:
                    done_shift = True
                if d < len(D) - 1:
                    d += 1
                else:
                    done_work = True
                    done_shift = True
        else:
            while not done_break:
                if sum(X[n, s, k, len(D) - d - 1].x for s in S for k in K) < 0.1:
                    break_stretch[n] += 1
                else:
                    done_break = True
                if d < len(D) - 1:
                    d += 1
                else:
                    done_break = True

    filename = 'H{}-{}Base.txt'.format(week + 1, scen)
    write_hist(path, filename, scen, week + 1, S, N, K, nurse_names, shift_names, \
                tot_ass, tot_weekend, last_shift, shift_stretch, \
                work_stretch, break_stretch)

    # store solution for current week in file format outlined in rules of competition
    sol_file_name = 'W{}- Solution.txt'.format(week)
    day_names = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
    with open(os.path.join(path, sol_file_name), 'w') as f:
        assignments = sum(X[n, s, k, d].x for (n, s, k, d) in X)
        lines = 'Assignments = {}\n'.format(assignments)
        for n in N:
            for d in D:
                for s in S:
                    for k in K:
                        if X[n, s, k, d].x > 0.9:
                            lines += '{} {} {} {}\n'.format(nurse_names[s], day_names[d], shift_names[s], skills[k])
        f.writelines(lines)

    # store values of solved variables in dictionary for later use
    XV = {(n, s, k, d): X[n, s, k, d].x for (n, s, k, d) in X}

    YV = {n: Y[n].x for n in N}

    CS1V = {(s, k, d): CS1[s, k, d].x for (s, k, d) in CS1}

    CS2AV = {(n, s, d): CS2A[n, s, d].x for (n, s, d) in CS2A}
    CS2BV = {(n, s, d): CS2B[n, s, d].x for (n, s, d) in CS2B}

    CS2CV = {(n, d): CS2C[n, d].x for (n, d) in CS2C}
    CS2DV = {(n, d): CS2D[n, d].x for (n, d) in CS2D}

    CS3AV = {(n, d): CS3A[n, d].x for (n, d) in CS3A}
    CS3BV = {(n, d): CS3B[n, d].x for (n, d) in CS3B}

    CS4V = {(n, d): CS4[n, d].x for (n, d) in CS4}

    CS5V = {n: CS5[n].x for n in CS5}

    CS6V = {n: CS6[n].x for n in CS6}
    CS62V = {n: CS62[n].x for n in CS62}

    CS7V = {n: CS7[n].x for n in CS7}
    CS72V = {n: CS72[n].x for n in CS72}

    CS8V = {(s, k, d): CS8[s, k, d].x for (s, k, d) in CS8}

    print('Penalties for CS62, week', week)
    for n in N:
        print('Nurse {}: #{}, {}'.format(n, CS62V[n], 9.9 * CS62V[n]))


    return XV, YV, CS1V, CS2AV, CS2BV, CS2CV, CS2DV, CS3AV, CS3BV, CS4V, CS5V, CS6V, CS7V, CS62V, CS72V, CS8V, m, coverage, requests
        
    
def solve_scen(scen, hist_num, week_nums):
    """
    Solve a scenario instance using method outlined in initial paper

    Parameters
    ----------
    scen : str
        Name of scenario instance

    hist_num : int
        Number label of history file used (appears at end of original file name)

    week_nums : list
        List of integer labels of week data files used in this instance

    Returns
    -------
    XVS, YVS, CS1VS, CS4VS, CS5VS, CS6VS, CS62VS, CS7VS, CS72VS, CS8VS- dictionaries storing solved variables for each week of scenario instance
    """
    W, N, S, K, skills, shift_names, shift_details, banned, work_blocks, shift_blocks, \
    break_blocks, contract_types, max_weekends, complete_weekend, nurse_names, \
        nurse_contracts, nurse_skills, orig_shift_blocks, orig_break_blocks = read_scenario(scen, 'Sc-{}'.format(scen))
    # folder we store data and results in 
    # W = range(1)
    sol_folder = 'Solution to H_{}-WD_{}'.format(hist_num, week_nums[0])
    for i in range(1, len(week_nums)):
        sol_folder += '-{}'.format(week_nums[i])

    # check if paths correct/exist- if not, make new path
    orig_path = os.path.join(DATAPATH, scen)
    new_path = os.path.join(orig_path, 'Rep Sols', sol_folder)

    if not os.path.isdir(orig_path):
        os.makedirs(orig_path)

    try:
        open(os.path.join(orig_path, 'Sc-{}.txt'.format(scen)))
    except FileNotFoundError:
        orig_path = DATAPATH
    
    if not os.path.isdir(os.path.join(orig_path, 'Rep Sols')):
        os.makedirs(os.path.join(orig_path, 'Rep Sols'))
    if not os.path.isdir(new_path):
        os.makedirs(new_path)

    # copy scenario data into local directory
    shutil.copy(os.path.join(orig_path, 'Sc-{}.txt'.format(scen)), \
                os.path.join(new_path, 'Sc-{}.txt'.format(scen)))

    # W = range(1)
    # copy week data into local directory
    for w in week_nums:
        shutil.copy(os.path.join(orig_path, 'WD-{}-{}.txt'.format(scen, w)), \
                    os.path.join(new_path, 'WD-{}-{}.txt'.format(scen, w)))
    # copy history file into local directory
    shutil.copy(os.path.join(orig_path, 'H0-{}-{}.txt'.format(scen, hist_num)), \
                os.path.join(new_path, 'H0-{}Base.txt'.format(scen)))

    if os.path.isfile(os.path.join(new_path, 'LogFileBase.txt')):
        os.remove(os.path.join(new_path, 'LogFileBase.txt'))

    # dictionaries where weeks are keys and dict containing values of variables
    # for that week as values
    XVS = {}
    YVS = {}
    CS1VS = {}
    CS2AVS = {}
    CS2BVS = {}
    CS2CVS = {}
    CS2DVS = {}
    CS3AVS = {}
    CS3BVS = {}
    CS4VS = {}
    CS5VS = {}
    CS6VS = {}
    CS62VS = {}
    CS7VS = {}
    CS72VS = {}
    CS8VS = {}
    MS = {}

    num_weeks = len(W)
    coverage = {w: None for w in W}
    requests = {w: None for w in W}

    timeout = [0] * len(W)
    for w in W:
        start = time.time()
        XV, YV, CS1V, CS2AV, CS2BV, CS2CV, CS2DV, CS3AV, CS3BV, CS4V, CS5V, CS6V, CS7V, CS62V, CS72V, CS8V, \
            m, cov_data, req_data = \
            solve_week(new_path, scen, w, week_nums, W, N, S, K, skills, shift_names, \
            shift_details, banned, work_blocks, shift_blocks, break_blocks, \
            max_weekends, complete_weekend, nurse_names, nurse_contracts, nurse_skills)
        end = time.time()

        XVS[w] = XV
        YVS[w] = YV
        CS1VS[w] = CS1V
        CS2AVS[w] = CS2AV
        CS2BVS[w] = CS2BV
        CS2CVS[w] = CS2CV
        CS2DVS[w] = CS2DV
        CS3AVS[w] = CS3AV
        CS3BVS[w] = CS3BV
        CS4VS[w] = CS4V
        CS5VS[w] = CS5V
        CS6VS[w] = CS6V
        CS7VS[w] = CS7V
        CS62VS[w] = CS62V
        CS72VS[w] = CS72V
        CS8VS[w] = CS8V
        MS[w] = m

        coverage[w] = cov_data
        requests[w] = req_data

        if m.Runtime >= 2500:
            timeout[w] = 1

    # present results as a table of all assignments for all nurses across 
    # planning horizon and also display costs from violating soft constraints

    day_names = ['M', 'T', 'W', 'T', 'F', 'S', 'S']
    D = range(len(day_names))
    shift_letters = [shift_names[s][0] for s in S]

    # W = range(8)

    # store results in a document called ResultsBASE (to distinguish from 
    # results for block formulation)
    with open(os.path.join(new_path, 'ResultsBASE.txt'), 'w') as f:
        lines = ''

        num_ass = sum(XVS[w][n, s, k, d] for w in W[:num_weeks] for (n, s, k, d) in XVS[w])
        avRunTime = sum(MS[w].Runtime for w in W[:num_weeks])/len(W)
        lines += 'ASSIGNMENTS = {}\n'.format(round(num_ass))
        lines += 'TOTAL COST: {}\n'.format(round(sum(MS[w].objVal for w in W[:num_weeks])))
        lines += 'AVERAGE RUNTIME: {}\n\n'.format(round(avRunTime, 2))

        lines += '\t'
        for w in W:
            lines += '|'
            for d in D:
                lines += '{}|'.format(day_names[d])
            lines += '  '
        lines += '\n' + '-' * 74 + '\n'
        
        for n in N:
            lines += nurse_names[n] + '\t'
            for w in W[:num_weeks]:
                lines += '|'
                for d in D:
                    found = False
                    for s in S:
                        for k in K:
                            if XVS[w][n, s, k, d] > 0.9:
                                found = True
                                lines += '{}|'.format(shift_letters[s])
                                break
                    if not found:
                        lines += '-|'
                lines += '  '
            lines += '\n'
        lines += '-' * 74
        lines += '\n\nWEEKLY RESULTS\n'
            

        tot_1 = 0
        tot_2 = 0
        tot_3 = 0
        tot_4 = 0
        tot_5 = 0
        tot_6 = 0
        tot_7 = 0

        for w in W[:num_weeks]:
            lines += 'Week {}\n'.format(w)
            opt_cost = 0
            shift_cost = 0
            work_cost = 0
            break_cost = 0
            req_cost = 0
            complete_cost = 0
            tot_ass_cost = 0
            tot_weekend_cost = 0
            av_assignment_cost = 0
            av_weekend_cost = 0
            over_optimal_cost = 0

            for s in S:
                for k in K:
                    for d in D:
                        if CS1VS[w][s, k, d] > 0.9:
                            opt_cost += 30 * CS1VS[w][s, k, d]
                        if CS8VS[w][s, k, d] > 0.9:
                            over_optimal_cost += 11.9 * CS8VS[w][s, k, d]

            
            for n in N:
                if CS5VS[w][n] > 0.9:
                    complete_cost += 30
                if CS6VS[w][n] > 0.9:
                    tot_ass_cost += 20 * CS6VS[w][n]
                if CS7VS[w][n] > 0.9:
                    tot_weekend_cost += 30 * CS7VS[w][n]
                if CS62VS[w][n] > 0.9:
                    av_assignment_cost += 9.9 * CS62VS[w][n]
                if CS72VS[w][n] > 0.9:
                    av_weekend_cost += 9.9 * CS72VS[w][n]
                for d in D:
                    if CS2CVS[w][n, d] > 0.9:
                        work_cost += 30 * CS2CVS[w][n, d]
                    if CS2DVS[w][n, d] > 0.9:
                        work_cost += 30
                    if CS3AVS[w][n, d] > 0.9:
                        break_cost += 30 * CS3AVS[w][n, d]
                    if CS3BVS[w][n, d] > 0.9:
                        break_cost += 30
                    if CS4VS[w][n, d] > 0.9:
                        req_cost += 10
                    
                    for s in S:
                        if CS2AVS[w][n, s, d] > 0.9:
                            shift_cost += 15 * CS2AVS[w][n, s, d]
                        if CS2BVS[w][n, s, d] > 0.9:
                            shift_cost += 15

            lines += '-' * 24 + '\n'
            lines += 'OBJECTIVE VALUE: {}\n'.format(round(MS[w].objVal))
            lines += 'RUNTIME: {}\n'.format(round(MS[w].Runtime, 2))
            lines += '-' * 24 + '\n'
            lines += 'Shift Cost: {}\n'.format(round(shift_cost))
            lines += 'Work Cost: {}\n'.format(round(work_cost))
            lines += 'Break Cost: {}\n'.format(round(break_cost))
            lines += 'Below Optimality Cost: {}\n'.format(round(opt_cost))
            lines += 'Request Cost: {}\n'.format(round(req_cost))
            lines += 'Complete Weekend Cost: {}\n'.format(round(complete_cost))
            lines += 'Total Assignments Cost: {}\n'.format(round(tot_ass_cost))
            lines += 'Total Weekends Worked Cost: {}\n'.format(round(tot_weekend_cost))
            lines += 'Average Assignments: {}\n'.format(round(av_assignment_cost))
            lines += 'Average Weekends Worked: {}\n'.format(round(av_weekend_cost))
            lines += 'Over Optimal Coverage: {}\n'.format(round(over_optimal_cost))
            lines += '\n\n'

            tot_1 += round(opt_cost, 2)
            tot_2 += round(shift_cost + work_cost, 2)
            tot_3 += round(break_cost, 2)
            tot_4 += round(req_cost, 2)
            tot_5 += round(complete_cost, 2)
            tot_6 += round(tot_ass_cost, 2)
            tot_7 += round(tot_weekend_cost, 2)
        
        lines += 'S1: {}\n'.format(tot_1)
        lines += 'S2: {}\n'.format(tot_2)
        lines += 'S3: {}\n'.format(tot_3)
        lines += 'S4: {}\n'.format(tot_4)
        lines += 'S5: {}\n'.format(tot_5)
        lines += 'S6: {}\n'.format(tot_6)
        lines += 'S7: {}\n'.format(tot_7)
        
        f.writelines(lines)
    hist_name = 'H{}-{}Base'.format(hist_num, scen)

    tot_cost = eval_roster(scen, hist_num, week_nums, new_path, 'ResultsBASE.txt', 'Base', XVS, YVS, N, range(len(S) + 1), K, W[:num_weeks], D, \
        coverage, banned, nurse_skills, shift_details, shift_names, shift_blocks, break_blocks, \
        requests, complete_weekend, work_blocks, max_weekends)

    return tot_cost, round(avRunTime, 2) , timeout