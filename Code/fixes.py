import os

from data import read_scenario, read_week, read_hist, DATAPATH
from initMIP import solve_scen
from block_generation import gen_master

# this just takes a solution from the original MIP and scores it using block costs
def block_score(scen, hist_num, week_nums, XVS, YVS, CS1VS, CS4VS, CS5VS, CS6VS, CS62VS, CS7VS, CS72VS, CS8VS):
    sol_folder = 'Solution to H_{}-WD_{}'.format(hist_num, week_nums[0])
    for i in range(1, len(week_nums)):
        sol_folder += '-{}'.format(week_nums[i])
    path = os.path.join(DATAPATH, scen, 'Rep Sols', sol_folder)

    filepath = os.path.join(path, 'ResultsBASE.txt')

    W, N, S, K, skills, shift_names, shift_details, banned, work_blocks, shift_blocks, \
    break_blocks, contract_types, max_weekends, complete_weekend, nurse_names, \
        nurse_contracts, nurse_skills, orig_shift_blocks, orig_break_blocks = read_scenario(scen, 'Sc-{}'.format(scen))


    blocks, contract_costs, assignments, shift_pos, successors, restriction_cost, work_cost = gen_master(W, \
        N, S, K, skills, shift_names, shift_details, banned, work_blocks, shift_blocks, \
        break_blocks, contract_types, max_weekends, complete_weekend, nurse_names, \
        nurse_contracts, nurse_skills, orig_shift_blocks, orig_break_blocks)


    block_costs = {(n, i): contract_costs[i, contract_types.index(nurse_contracts[n])]
                    for n in N for i in blocks}

    print((0, 0, 0, 0, 0, 1, 1) in blocks)

    D = range(7)

    SW = S
    S = range(len(SW) + 1)
    print('SW = ', SW)
    print('S = ', S)
    W = range(1)

    UVS = {w: {} for w in W}
    blocks = []

    coverage = {w: {} for w in W}
    requests = {w: {} for w in W}

    for w in W:
        for n in N:
            for d in D:
                if (d == 0 or sum(XVS[w][n, s, k, d - 1] for s in SW for k in K) < 0.1) and sum(XVS[w][n, s, k, d] for s in SW for k in K) > 0.9:
                    done_work = False
                    i = []
                    while not done_work:
                        if d + len(i) < len(D) and sum(XVS[w][n, s, k, d + len(i)] for s in SW for k in K) > 0.9:
                            for s in SW:
                                if sum(XVS[w][n, s, k, d + len(i)] for k in K) > 0.9:
                                    i.append(s)
                                    break
                        else:
                            done_work = True
                    i = tuple(i)
                    blocks.append(i)
                    UVS[w][n, i, d] = 1
                elif (d == 0 or sum(XVS[w][n, s, k, d - 1] for s in SW for k in K) > 0.9) and sum(XVS[w][n, s, k, d] for s in SW for k in K) < 0.1:
                    done_break = False
                    i = []
                    while not done_break:
                        if d + len(i) < len(D) and sum(XVS[w][n, s, k, d + len(i)] for s in SW for k in K) < 0.1:
                            i.append(len(SW))
                        else:
                            done_break = True
                    i = tuple(i)
                    blocks.append(i)
                    UVS[w][n, i, d] = 1
                    
        cov_data, req_data = read_week(scen, path, 'WD-{}-{}'.format(scen, \
                        week_nums[w]), SW, K, N, shift_names, skills, nurse_names)
        coverage[w] = cov_data
        requests[w] = req_data
    
    # Define costs of placing each block at start of week given history details
    overflow_costs = {w: {(i, n): 0 for i in blocks for n in N} for w in W}
    end_costs = {(i, n): 0 for i in blocks for n in N}

    for w in W:
        # read the previous week's history (week zero reads the initial history file)
        hist_name = 'H{}-{}'.format(w, scen)
        tot_ass, tot_wend, shift_hist, shift_len, work_len, break_len = read_hist(path, hist_name, S, N, shift_names)

        for (i, n) in overflow_costs[w]:
            for p in range(len(i)):
                if i[p] != len(SW) and (p == 0 or i[p - 1] != i[p]):
                    shift_done = False
                    count_shift = 1
                    while not shift_done:
                        if p + count_shift < len(i) and i[p + count_shift] == i[p]:
                            count_shift += 1
                        else:
                            shift_done = True
                    if p == 0 and i[0] == shift_hist[n]:
                        overflow_costs[w][i, n] += 15 * (max(shift_details[i[0]][0] - count_shift - shift_len[n], 0) + \
                                                        max(count_shift + shift_len[n] - shift_details[i[0]][1], 0))
                    elif p == 0 and i[0] != shift_hist[n] and shift_hist[n] != len(SW):
                        overflow_costs[w][i, n] += 15 * (max(shift_details[i[0]][0] - count_shift, 0) + \
                                                        max(count_shift - shift_details[i[0]][1], 0)) + \
                                                15 * max(shift_details[shift_hist[n]][0] - shift_len[n], 0)
                    elif (p != 0 or shift_hist[n] == len(SW)):
                        if len(i) < len(D) or p + count_shift < len(D):
                            overflow_costs[w][i, n] += 15 * (max(shift_details[i[p]][0] - count_shift, 0) + \
                                                            max(count_shift - shift_details[i[p]][1], 0))
                        else:
                            overflow_costs[w][i, n] += 15 * max(count_shift - shift_details[i[p]][1], 0)
            if i[0] != len(SW):
                if work_len[n] > 0:
                    overflow_costs[w][i, n] += 30 * (max(shift_blocks[n][0] - len(i) - work_len[n], 0) + \
                                                    max(len(i) + work_len[n] - shift_blocks[n][1], 0))
                else:
                    overflow_costs[w][i, n] += 30 * max(break_blocks[n][0] - break_len[n], 0) + \
                                            30 * (max(shift_blocks[n][0] - len(i), 0) + \
                                                    max(len(i) - shift_blocks[n][1], 0))
            else:
                if break_len[n] > 0:
                    overflow_costs[w][i, n] += 30 * (max(break_blocks[n][0] - len(i) - break_len[n], 0) + \
                                                    max(len(i) + break_len[n] - break_blocks[n][1], 0))
                else:
                    overflow_costs[w][i, n] += 30 * (max(shift_blocks[n][0] - work_len[n], 0)) + \
                                            30 * (max(break_blocks[n][0] - len(i), 0) + \
                                                    max(len(i) - break_blocks[n][1], 0)) + \
                                            15 * max(shift_details[shift_hist[n]][0] - shift_len[n], 0) 


    for (i, n) in end_costs:
        if i[-1] != len(SW):
            for p in range(len(i)):
                if p == 0 or i[p - 1] != i[p]:
                    done = False
                    count = 1
                    while not done:
                        if p + count < len(i) and i[p + count] == i[p]:
                            count += 1
                        else:
                            done = True
                    if i[p] != i[-1]:
                        end_costs[i, n] += 15 * (max(shift_details[i[p]][0] - count, 0) + \
                                                max(count - shift_details[i[p]][1], 0))
                    else:
                        end_costs[i, n] += 15 * max(count - shift_details[i[p]][1], 0)
            if len(i) > shift_blocks[n][1]:
                end_costs[i, n] += 30 * (len(i) - shift_blocks[n][1])
        elif i[-1] == len(SW) and len(i) > break_blocks[n][1]:
            end_costs[i, n] += 30 * (len(i) - break_blocks[n][1])


    last_blocks = [[(n, i, d) for (n, i, d) in UVS[w] if d + len(i) == 7] for w in W]
    
    print(blocks)
    block_eval_costs = 0
    for w in W:
        print(UVS[w])
        bc = sum(block_costs[n, i] * UVS[w][n, i, d] for (n, i, d) in UVS[w] if d > 0 and d + len(i) < len(D))
        S1 = 30 * sum(CS1VS[w][s, k, d] for (s, k, d) in CS1VS[w])
        S4 = 10 * sum(CS4VS[w][n, d] for (n, d) in CS4VS[w])
        S5 = sum(30 * CS5VS[w][n] for n in N)
        S6 = sum(20 * CS6VS[w][n] for n in N)
        S7 = sum(30 * CS7VS[w][n] for n in N)
        S8 = 11.9 * sum(CS8VS[w][s, k, d] for (s, k, d) in CS8VS[w])
        S62 = 9.9 * sum(CS62VS[w][n] for n in N)
        S72 = 9.9 * sum(CS72VS[w][n] for n in N)
        EC = sum(end_costs[i, n] * UVS[w][n, i, d] for (n, i, d) in UVS[w] if d + len(i) == len(D))
        OF = sum(overflow_costs[w][i, n] * UVS[w][n, i, 0] for (i, n) in overflow_costs[w] if (n, i, 0) in UVS[w])

        
        for (n, i, d) in UVS[w]:
            if d == 0:
                print(overflow_costs[w][i, n])
                print(block_costs[n, i])
                print(('n', n, 'i', i, 'd', d, 'bc', block_costs[n, i], 'OF', overflow_costs[w][i, n]))
            elif d + len(i) == 7:
                print(('n', n, 'i', i, 'd', d, 'bc', block_costs[n, i], 'EC', end_costs[i, n]))
            else:
                print(('n', n, 'i', i, 'd', d, 'bc', block_costs[n, i]))
        print('\nWeek: ', w)
        print('BC: ', bc)
        print('OF: ', OF)
        print('EC: ', EC)
        print('S1: ', S1)
        print('S4: ', S4)
        print('S5: ', S5)
        print('S6: ', S6)
        print('S62: ', S62)
        print('S7: ', S7)
        print('S72: ', S72)
        print('S8: ', S8)
        week_cost = bc + S1 + S4 + S5 + S6 + S7 + S8 + S62 + S72 + EC + OF
        print('Tot: ', week_cost)
        block_eval_costs += week_cost
    return block_eval_costs

def main():
    # scen = 'n005w4'
    # hist_num = 0
    # week_nums = [1, 2, 3, 3]

    # scen = 'n021w4'
    # hist_num = 0
    # week_nums = [5, 4, 1, 2]

    scen = 'n035w8'
    hist_num = 0
    # week_nums = [6]
    week_nums = [6, 2, 9, 8, 7, 7, 9, 8]

    # fix_blocks(scen, hist_num, week_nums)

    XVS, YVS, CS1VS, CS4VS, CS5VS, CS6VS, CS62VS, CS7VS, CS72VS, CS8VS = solve_scen(scen, hist_num, week_nums)

    block_eval_costs = block_score(scen, hist_num, week_nums, XVS, YVS, CS1VS, CS4VS, CS5VS, CS6VS, CS62VS, CS7VS, CS72VS, CS8VS)
    print('Block Eval Cost: ', block_eval_costs)

if __name__ == '__main__':
    main()