from gurobipy import *
import os
import math
import shutil
import sys

from block_mip import solve_block_scen, solve_block_week
from data import read_scenario, read_week, read_hist, write_hist, DATAPATH, BENCHMARK_TIMES
from block_generation import gen_master, block_carry_costs
from evaluate import eval_roster, eval_roster_file
import numpy as np
from scipy.optimize import dual_annealing

def model_instance(w, scen='n005w4', hist_num=0, week_nums=[1, 2, 3, 3]):
    tot_cost, avRuntime, timeout = solve_block_scen(scen, hist_num, week_nums, \
        w6=w[0], w7=w[1], w8=w[2], w6q=w[3], w11=w[4])
    print(tot_cost)
    return tot_cost

def tune_weights(scen, hist_num, week_nums, w6range, w7range, w8range, w6qrange, \
    w11range):

    bounds = [w6range, w7range, w8range, w6qrange, w11range]
    w_opt = dual_annealing(model_instance, bounds=bounds, args=(scen, hist_num, week_nums), maxiter=2, seed=1234, no_local_search=True, x0=[9.9, 9.9, 11.9, 1, 5])

    print(w_opt)

    print('\nOptimal Weights for Objective Function Are:\n')
    print('w6 = ', w_opt.x[0])
    print('w7 = ', w_opt.x[1])
    print('w8 = ', w_opt.x[2])
    print('w6q = ', w_opt.x[3])
    print('w11 = ', w_opt.x[4])

    return w_opt

    

def save_model(scen, hist_num, week_nums):
    """
    Solve the block formulation MIP implementation for a given week in the planning horizon

    Parameters
    ----------
    path : str
        Path of the folder containing relevant data

    scen : str
        Name of the scenario instance

    week : int
        Week number in planning horizon

    blocks : list
        List of feasible blocks

    block_cost : dict
        Dictionary mapping each nurse/block pair to the cost of assigning that nurse to that block

    shift_pos : dict
        Dictionary mapping (block, shift, pos) to 1 if block has shift at index pos, else 0
    
    successors : dict
        Dictionary mapping each block to list of valid successors

    week_nums : list
        List of week data file mumbers used in scenario instance

    W : list
        List of weeks

    N : list
        List of nurses

    S : list
        List of shifts

    K : list
        List of skills

    skills : list
        List of string names of skills

    shift_names : list
        List of string names of shifts

    banned : List
        List of tuples of banned shift sequence pairs

    work_blocks : list
        List of maximum numbers of assignments for each nurse

    shift_blocks : list
        List of min/max number of consecutive work assignments for each nurse

    break_blocks : list
        List of min/max number of consecutive breaks for each nurse

    max_weekends : list
        List of maximum numbers of weekends that can be worked by each nurse in horizon

    nurse_names : list
        List of string names of each nurse

    nurse_skills : dict
        Dictionary mapping each nurse to list of their skill types

    Returns
    -------
    XV, YV, CS1V, CS2AV, CS2BV, CS2CV, CS2DV, CS3AV, CS3BV, CS4V, CS5V, CS6V, CS7V, CS62V, CS72V, CS8V: dictionaries of solved values for all variables

    m: model

    overflow_costs, end_costs- Cost data relating to overflow/end of week, returned for convenience later

    coverage, requests: week data returned for convenience later
    """

    W, N, S, K, skills, shift_names, shift_details, banned, work_blocks, \
        shift_blocks, break_blocks, contract_types, max_weekends, \
        complete_weekend, nurse_names, nurse_contracts, nurse_skills, \
        orig_shift_blocks, orig_break_blocks = read_scenario(scen, 'Sc-{}'.format(scen))

    #generate set of feasible blocks
    blocks, contract_costs, assignments, shift_pos, successors, work_cost, restrictions_contract = gen_master(W, \
        N, S, K, skills, shift_names, shift_details, banned, work_blocks, shift_blocks, \
        break_blocks, contract_types, max_weekends, complete_weekend, nurse_names, \
        nurse_contracts, nurse_skills, orig_shift_blocks, orig_break_blocks)

    # change block costs to be indexed by nurse rather than contract type
    block_costs = {(n, i): contract_costs[i, contract_types.index(nurse_contracts[n])]
                    for n in N for i in blocks}

    restrictions = {(n, i): restrictions_contract[i, contract_types.index(nurse_contracts[n])]
        for n in N for i in blocks}

    # folder we store data and results in 
    sol_folder = 'Solution to H_{}-WD_{}'.format(hist_num, week_nums[0])
    for i in range(1, len(week_nums)):
        sol_folder += '-{}'.format(week_nums[i])

    # check if paths correct/exist- if not, make new path
    orig_path = os.path.join(DATAPATH, scen)
    new_path = os.path.join(orig_path, 'Rep Sols', sol_folder)

    if not os.path.isdir(orig_path):
        os.makedirs(orig_path)

    try:
        open(os.path.join(orig_path, 'Sc-{}.txt'.format(scen)))
    except FileNotFoundError:
        orig_path = DATAPATH
    
    if not os.path.isdir(os.path.join(orig_path, 'Rep Sols')):
        os.makedirs(os.path.join(orig_path, 'Rep Sols'))
    if not os.path.isdir(new_path):
        os.makedirs(new_path)

    # copy scenario data into local directory
    shutil.copy(os.path.join(orig_path, 'Sc-{}.txt'.format(scen)), \
                os.path.join(new_path, 'Sc-{}.txt'.format(scen)))
    # copy week data into local directory
    for w in week_nums:
        shutil.copy(os.path.join(orig_path, 'WD-{}-{}.txt'.format(scen, w)), \
                    os.path.join(new_path, 'WD-{}-{}.txt'.format(scen, w)))
    # copy history file into local directory
    shutil.copy(os.path.join(orig_path, 'H0-{}-{}.txt'.format(scen, hist_num)), \
                os.path.join(new_path, 'H0-{}Block.txt'.format(scen)))

    if os.path.isfile(os.path.join(new_path, 'LogFileBlock.txt')):
        os.remove(os.path.join(new_path, 'LogFileBlock.txt'))

    av_cover = {(s, k): [0, 0] for s in S for k in K}

    D = range(7)

    # print('S = ', S)
    # print('K = ', K)
    # load data for the week
    print(('scen = ', scen, 'H = ', hist_num, 'WD = ', week_nums))
    # Set of working shifts
    SW = S
    # Set of all shifts
    S = range(len(SW) + 1)
    for week in range(len(week_nums)):
        print('w = ', week)
        coverage, requests = read_week(scen, new_path, 'WD-{}-{}'.format(scen, \
                        week_nums[week]), SW, K, N, shift_names, skills, nurse_names)
        hist_name = 'H{}-{}Block'.format(week, scen)
        # read the previous week's history (week zero reads the initial history file)
        tot_ass, tot_weekend, shift_hist, shift_len, work_len, break_len = read_hist(new_path, hist_name, SW, N, shift_names)

        # Define costs of placing each block at start of week given history details
        overflow_costs, end_costs = block_carry_costs(blocks, SW, N, shift_details, \
        shift_blocks, break_blocks, shift_hist, shift_len, work_len, break_len)

        has_skill = {(n, k): (1 if k in nurse_skills[n] else 0) for n in N for k in K}

        for (s, k) in av_cover:
            av_cover[s, k][0] = (av_cover[s, k][0] + coverage[s, k, 0][0])/(week + 1)
            av_cover[s, k][1] = (av_cover[s, k][1] + coverage[s, k, 0][1])/(week + 1)

        m = Model('INRC Block')
        m.setParam("TimeLimit", 600)
        m.setParam('LogFile', os.path.join(new_path, 'LogFileBlock.txt'))
        # m.setParam('Method', 2)
        m.setParam('OutputFlag', 0)

        #variables
        # X[n, s, k, d] = 1 if nurse n works shift s with skill k on day d, 0 otherwise
        X = {(n, s, k, d): m.addVar(vtype=GRB.BINARY) 
                for n in N for s in S for k in K for d in D}
        
        # Y[n] = 1 if nurse n words on the weekend
        Y = {n: m.addVar(vtype=GRB.BINARY) for n in N}

        # U[n, i, d] = 1 if we place block i for nurse n starting on day d, 0 otherwise
        U = {(n, i, d): m.addVar(vtype = GRB.BINARY) 
                for n in N for i in blocks for d in D if len(i) + d <= 7}

        # variables relating to soft constraints
        # number of missing nurses for optimal coverage of shift s, skill k, day d 
        CS1 = {(s, k, d): m.addVar(vtype=GRB.INTEGER) for s in SW for k in K for d in D}

        # 1 if assignment on day d violates request for nurse n, 0 otherwise
        CS4 = {(n, d): m.addVar(vtype=GRB.BINARY) for n in N for d in D}

        # 1 if nurse n violates complete weekend constraint, 0 otherwise
        CS5 = {n: m.addVar(vtype=GRB.BINARY) for n in N}

        # number of total shifts outside the allowed bounds for nurse n
        CS6 = {n: m.addVar(vtype=GRB.INTEGER) for n in N}
        # number of total shifts outside the allowed bounds for average assignments 
        # for nurse n
        CS62 = {n: m.addVar(vtype = GRB.INTEGER) for n in N}

        # number of weekends worked outside of the allowed bounds for nurse n
        CS7 = {n: m.addVar(vtype=GRB.INTEGER) for n in N}
        # number of weekends worked outside of the allowed average bounds for nurse n
        CS72 = {n: m.addVar(vtype=GRB.INTEGER) for n in N}
        
        # Number of nurses assigned above optimal level
        CS8 = {(s, k, d): m.addVar(vtype = GRB.INTEGER) for s in SW for k in K for d in D}

        last_blocks = [(i, d) for i in blocks for d in D if d + len(i) == len(D)]
        mid_blocks = [(i, d) for i in blocks for d in D if d > 0 and d + len(i) < len(D)]

        if week != W[-1]:
            # CS11 = {(s, k): m.addVar(vtype=GRB.BINARY) for s in SW for k in K}
            CS11 = {(s, k): m.addVar(vtype=GRB.INTEGER) for s in SW for k in K}

            m.setObjective((quicksum(block_costs[n, i] * U[n, i, d] 
                                for n in N for (i, d) in mid_blocks) + \
                        quicksum(end_costs[i, n] * U[n, i, d] for n in N for (i, d) in last_blocks) + \
                        quicksum(overflow_costs[i, n] * U[n, i, 0] for (i, n) in overflow_costs if (n, i, 0) in U) + \
                        30 * quicksum(CS1[s, k, d] for (s, k, d) in CS1) + \
                        10 * quicksum(CS4[n, d] for (n, d) in CS4) + \
                        30 * quicksum(CS5[n] for n in N) +\
                        20 * quicksum(CS6[n] for n in N) + \
                        30 * quicksum(CS7[n] for n in N)) + \
                        11.9 * quicksum(CS8[s, k, d] 
                                for (s, k, d) in CS8) + \
                        9.9 * quicksum(CS62[n] for n in N) + \
                        1 * quicksum(CS62[n] * CS62[n] for n in N) + \
                        9.9 * quicksum(CS72[n] for n in N) + \
                        5 * quicksum(CS11[s, k] for (s, k) in CS11),
                        GRB.MINIMIZE)
        else:
            m.setObjective(quicksum(block_costs[n, i] * U[n, i, d] 
                                    for n in N for (i, d) in mid_blocks) + \
                            quicksum(end_costs[i, n] * U[n, i, d] for n in N for (i, d) in last_blocks) + \
                            quicksum(overflow_costs[i, n] * U[n, i, 0] for (i, n) in overflow_costs if (n, i, 0) in U) + \
                            30 * quicksum(CS1[s, k, d] for (s, k, d) in CS1) + \
                            10 * quicksum(CS4[n, d] for (n, d) in CS4) + \
                            30 * quicksum(CS5[n] for n in N) +\
                            20 * quicksum(CS6[n] for n in N) + \
                            30 * quicksum(CS7[n] for n in N),
                            GRB.MINIMIZE)

        if week != W[-1]:
            NextWeekDemandRestrictionsTight = {(s, k): m.addConstr(quicksum(has_skill[n, k] * restrictions[n, i][s] * U[n, i, d] 
                for n in N for (i, d) in last_blocks) >= math.ceil(max(av_cover[s, k][0], 0 * av_cover[s, k][1])) - CS11[s, k])
            for s in SW for k in K}

        OverlapBlocks = {(n, s, d): [(i, p) for i in blocks for p in range(len(i)) 
                if i[p] == s and (n, i, d - p) in U]
        for n in N for s in S for d in D}

        BlocksToDays = {(n, s, d): m.addConstr(quicksum(U[n, i, d - p] * shift_pos[i, s, p]
                for (i, p) in OverlapBlocks[n, s, d]) == 
                quicksum(X[n, s, k, d] for k in K))
        for (n, s, d) in OverlapBlocks}

        OFConflicts = {n: [i for i in blocks if (shift_hist[n], i[0]) in banned] for n in N}
        
        BlockSuccessionOF = {n: m.addConstr(quicksum(U[n, i, 0] for i in OFConflicts[n]) == 0) for n in N}

        work_set = [i for i in blocks if i[0] != len(SW)]
        break_set = [i for i in blocks if i[0] == len(SW)]

        valid_work_pred = {d: [i for i in work_set if d - len(i) >= 0] for d in D[1:]}
        valid_break_succ = {d: [i for i in break_set if d + len(i) <= 7] for d in D}

        valid_work_succ = {d: [i for i in work_set if d + len(i) <= 7] for d in D}
        valid_break_pred = {d: [i for i in break_set if d - len(i) >= 0] for d in D[1:]}

        BlockWorkFlow = {(n, d): m.addConstr(
            quicksum(U[n, i, d - len(i)] for i in valid_work_pred[d]) <= 
            quicksum(U[n, j, d] for j in valid_break_succ[d]))
        for n in N for d in D[1:]}

        BlockBreakFlow = {(n, d): m.addConstr(
            quicksum(U[n, i, d - len(i)] for i in valid_break_pred[d]) <= 
            quicksum(U[n, j, d] for j in valid_work_succ[d]))
        for n in N for d in D[1:]}

        OneAssignmentPerDay = {(n, d): m.addConstr(quicksum(X[n, s, k, d] 
                for s in S for k in K) == 1)
        for n in N for d in D}
        
        MinCover = {(s, k, d): m.addConstr(
                quicksum(X[n, s, k, d] for n in N) >= coverage[s, k, d][0])
        for s in SW for k in K for d in D}


        HasSkill = {(n, s, k, d): m.addConstr(
                X[n, s, k, d] == 0)
        for (n, s, k, d) in X if k not in nurse_skills[n]}


        SubOPtimal = {(s, k, d): m.addConstr(
                quicksum(X[n, s, k, d] for n in N) >= \
                coverage[s, k, d][1] - CS1[s, k, d])
        for (s, k, d) in CS1}

        ShiftRequests = {(n, s, d): m.addConstr(
                    quicksum(X[n, s, k, d] for k in K) <= CS4[n, d])
        for n in N for s in SW for d in D if requests[n, s, d] == 1}

        # S5
        Weekend1 = {(n, d): m.addConstr(quicksum(X[n, s, k, d] 
                    for s in SW for k in K) <= Y[n])
        for n in N for d in [5, 6]}

        Weekend2 = {n: m.addConstr(quicksum(X[n, s, k, 5] + X[n, s, k, 6] 
                    for s in SW for k in K) >=
                    2 * Y[n] - CS5[n])
        for n in N if complete_weekend[n] == 1}

        # S6
        TotAssignmentsU = {n: m.addConstr(quicksum(X[n, s, k, d] 
                    for s in SW for k in K for d in D) <= 
                    max(work_blocks[n][1] - tot_ass[n], 0) + CS6[n])
        for n in N}

        TotAssignmentsL = {n: m.addConstr(quicksum(X[n, s, k, d] 
                        for s in SW for k in K for d in D) >= 
                        min(work_blocks[n][0] - \
                        len(D) * (len(W) - week), len(D)) - CS6[n])
        for n in N}

        # S7
        TotWeekends = {n: m.addConstr(min(tot_weekend[n], max_weekends[n]) + Y[n] <= 
                        max_weekends[n] + CS7[n])
        for n in N}

        # Extensions

        # S8*
        OverStaffing = {(s, k, d): m.addConstr(quicksum(X[n, s, k, d] for n in N) <= 
                        coverage[s, k, d][1] + CS8[s, k, d])
        for s in SW for k in K for d in D}

        AvAssU = {n: m.addConstr(tot_ass[n] + quicksum(X[n, s, k, d] 
                    for s in SW for k in nurse_skills[n] for d in D) <= 
                    math.floor(work_blocks[n][1] * (week + 1)/len(W)) + CS62[n])
        for n in N}

        AvRemAssU = {n: m.addConstr(quicksum(X[n, s, k, d] 
                        for s in SW for k in nurse_skills[n] for d in D) <= 
                        math.floor((work_blocks[n][1] - tot_ass[n]) * 1/(len(W) - week)) + CS62[n])
        for n in N}

        # S7*
        AvWeekends = {n: m.addConstr(Y[n] <= 
                        math.floor((max(max_weekends[n] - tot_weekend[n], 0)) * \
                        1/(len(W) - week)) + CS72[n]) 
        for n in N}

        # m.optimize()
        print('Done opt')
        
        m.write(os.path.join(new_path, 'model-w{}-{}.rlp'.format(week, scen)))

        

def tune_weeks(instances, timelim, dest):
    for i in range(len(instances)):
        save_model(instances[i][0], instances[i][1], instances[i][2])
        sol_folder = 'Solution to H_{}-WD_{}'.format(instances[i][1], instances[i][2][0])
        for j in range(1, len(instances[i][2])):
            sol_folder += '-{}'.format(instances[i][2][j])
            sol_path = os.path.join(DATAPATH, instances[i][0], 'Rep Sols', sol_folder)
        for w in range(len(instances[i][2])):
            m = read(os.path.join(sol_path, 'model-w{}-{}.rlp'.format(w, instances[i][0])))
            m.setParam('TuneTimeLimit', timelim)
            m.setParam('TuneTrials', 5)
            m.tune()
            week_str = str(instances[i][2][0])
            for week in range(1, len(instances[i][2])):
                week_str += '-' + str(instances[i][2][week])
            m.write(os.path.join(DATAPATH, 'Tuning', 'tune{}-H{}-WD-{}_{}.prm'.format(instances[i][0], instances[i][1], week_str, w)))
            
def main():
    instances = [('n005w4', 0, [1, 2, 3, 3]), ('n005w4', 1, [5, 3, 1, 0]), ('n005w4', 2, [6, 7, 8, 9]),
                ('n012w8', 0, [3, 5, 0, 2, 0, 4, 5, 2]), ('n-12w8', 1, [7, 7, 0, 8, 9, 3, 2, 6]), 
                ('n12w8', 2, [4, 5, 6, 7, 2, 1, 2, 1]), 
                ('n021w4', 0, [5, 4, 1, 2]), ('n021w4', 1, [0, 6, 1, 6]), 
                ('n021w4', 2, [8, 1, 4, 3])]
    timelim = 1800
    dest = os.path.join(DATAPATH, 'Tuning')
    if not os.path.isdir(dest):
        os.mkdir(dest)
    tune_weeks(instances, timelim, dest)
    
    # for i in range(0, 1): #len(instances)):
    #     tune_model(instances[i][0], instances[i][1], instances[i][2])
    
    # scen = 'n005w4'
    # hist_num = 0
    # week_nums = [1, 2, 3, 3]
    # hist_num = 1
    # week_nums = [5, 3, 1, 0]
    # bounds = [0, 20]
    # w_opt = tune_weights(scen, hist_num, week_nums, bounds, bounds, bounds, bounds, bounds)

if __name__ == '__main__':
    # main()
    # def model_instance(w, scen='n005w4', hist_num=0, week_nums=[1, 2, 3, 3]):
    # tot_cost, avRuntime, timeout = solve_block_scen(scen, hist_num, week_nums, \
    #     w6=w[0], w7=w[1], w8=w[2], w6q=w[3], w11=w[4])
    # print(tot_cost)
    # return tot_cost
    print(sys.argv)

    w6 = float(sys.argv[1])
    w7 = float(sys.argv[2])
    w8 = float(sys.argv[3])
    w6q = float(sys.argv[4])
    w11 = float(sys.argv[5])

    w = [w6, w7, w8, w6q, w11]

    scen = str(sys.argv[6])
    hist_num = int(sys.argv[7])
    week_nums = []
    for i in range(8, len(sys.argv)):
        week_nums.append(int(sys.argv[i]))

    tot_cost = model_instance(w, scen=scen, hist_num=hist_num, week_nums=week_nums)