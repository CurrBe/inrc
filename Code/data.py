import os

# will need to change this to match desired directory
DATAPATH = os.path.join('C:', '\\Users', 'CurrB', 'OneDrive', 'Documents', '2020 Honours', 'INRC', 'Data', 'Hidden', 'hidden-TXT')
# DATAPATH = os.path.join('C:', '\\Users', 'CurrB', 'OneDrive', 'Documents', '2020 Honours', 'INRC', 'Data', 'Test')
# DATAPATH = os.path.join('C:', '\\Users', 'CurrB', 'OneDrive', 'Documents', '2020 Honours', 'INRC', 'Data', 'Late')

BENCHMARK_TIMES = {30: 85.45, 40: 149.53, 50: 213.61, 80: 405.87, 100: 534.04, 120: 662.2}

def read_scenario(scen_name, filename):
    """Read the scenario data

    Parameters
    ----------
    scen_name : str
        Name of the scenario

    filename : str
        Name of the file containing scenario data

    Returns
    -------
    Data structures used throughout the whole scenario
    """
    try:
        open(os.path.join(DATAPATH, scen_name, filename + '.txt'), 'r')
        path = os.path.join(DATAPATH, scen_name)
    except FileNotFoundError:
        path = DATAPATH
        
    with open(os.path.join(path, filename + '.txt'), 'r') as f:
        raw_data = f.readlines()
    
    num_weeks = int(raw_data[2].strip().split()[-1])
    num_skills = int(raw_data[4].strip().split()[-1])

    # Set of Weeks
    W = range(num_weeks)
    # Set of skills
    K = range(num_skills)

    # Skill names
    skills = []
    for i in range(num_skills):
        skills.append(raw_data[5 + i].strip().split()[-1])
    
    num_shifts = int(raw_data[6 + num_skills].strip().split()[-1])
    # Set of shifts
    S = range(num_shifts)

    shift_names = []
    # Contains min/max number of consecutive shifts of each type that can be worked
    shift_details = []
    # Contains banned shift sequences
    banned = []
    for i in range(num_shifts):
        info = raw_data[7 + num_skills + i].strip().split()
        shift_type = info[0]
        shift_names.append(shift_type)

        ass_details = eval(info[1])
        shift_details.append(ass_details)

        banned_info = raw_data[9 + num_skills + num_shifts + i].strip().split()
        prec_type = banned_info[0]
        num_banned = int(banned_info[1])
        for j in range(num_banned):
            banned.append((i, shift_names.index(banned_info[2 + j])))

    shift_names.append('None')

    num_contracts = int(raw_data[10 + num_skills + 2 * num_shifts].strip().split()[-1])

    # Contains min/max number of total working assignments for each nurse across horizon
    work_blocks = []
    # Contains min/max number of consecutive working days for each nurse
    shift_blocks = []
    # Contains min/max number of consecutive break shifts for each nurse
    break_blocks = []
    # List of all contract types
    contract_types = []
    # Max number of weekends for each nurse
    max_weekends = []
    # Stores whether or not each nurse has the complete weekend constraint
    complete_weekend = []
    for i in range(num_contracts):
        contract_info = raw_data[11 + num_skills + 2 * num_shifts + i].strip().split()
        contract_type = contract_info[0]
        contract_types.append(contract_type)

        work_constr = eval(contract_info[1])
        work_blocks.append(work_constr)

        shift_contr = eval(contract_info[2]) 
        shift_blocks.append(shift_contr)

        break_constr = eval(contract_info[3])
        break_blocks.append(break_constr)

        max_weekends.append(int(contract_info[4]))
        complete_weekend.append(int(contract_info[5]))

    num_nurses = int(raw_data[12 + num_skills + 2 * num_shifts + num_contracts].strip().split()[-1])
    # Set of nurses
    N = range(num_nurses)

    nurse_names = []
    nurse_contracts = []
    nurse_skills = []

    for n in N:
        nurse_info = raw_data[13 + num_skills + 2 * num_shifts + num_contracts + n].strip().split()

        nurse_names.append(nurse_info[0])
        nurse_contracts.append(nurse_info[1])

        skill_number = int(nurse_info[2])

        skill_list = []
        for k in range(skill_number):
            skill_list.append(skills.index(nurse_info[3 + k]))
        nurse_skills.append(skill_list)

    #fix indexing of shift blocks to depend on nurse, not contract
    orig_shift_blocks = shift_blocks
    new_shift_blocks = [None] * len(N)
    for n in N:
        new_shift_blocks[n] = shift_blocks[contract_types.index(nurse_contracts[n])]
    shift_blocks = new_shift_blocks

    #fix indexing of break blocks to depend on nurse, not contract
    orig_break_blocks = break_blocks
    new_break_blocks = [None] * len(N)
    for n in N:
        new_break_blocks[n] = break_blocks[contract_types.index(nurse_contracts[n])]
    break_blocks = new_break_blocks

    #fix indexing of work blocks to depend on nurse, not contract
    orig_work_blocks = work_blocks
    new_work_blocks = [None] * len(N)
    for n in N:
        new_work_blocks[n] = work_blocks[contract_types.index(nurse_contracts[n])]
    work_blocks = new_work_blocks

    #fix indexing of complete weekend to depend on nurse, not contract
    orig_complete_weekend = complete_weekend
    new_complete_weekend = [None] * len(N)
    for n in N:
        new_complete_weekend[n] = complete_weekend[contract_types.index(nurse_contracts[n])]
    complete_weekend = new_complete_weekend

    #fix indexing of max weekends to depend on nurse, not contract
    orig_max_weekends = max_weekends
    new_max_weekends = [None] * len(N)
    for n in N:
        new_max_weekends[n] = max_weekends[contract_types.index(nurse_contracts[n])]
    max_weekends = new_max_weekends

    return W, N, S, K, skills, shift_names, shift_details, banned, work_blocks, \
            shift_blocks, break_blocks, contract_types, max_weekends, \
            complete_weekend, nurse_names, nurse_contracts, nurse_skills, orig_shift_blocks, orig_break_blocks

def read_week(scen_name, path, filename, S, K, N, shift_names, skills, nurse_names):
    """Read the data for a given week in the planning horizon

    Parameters
    ----------
    scen_name : str
        Name of the scenario instance

    path : str
        Path containing the problem data

    filename : str
        Name of the file containing week data

    S : list
        List of shifts

    K : list
        List of skills

    N : list
        List of Nurses

    shift_names : list
        List of names of shifts

    skills : list
        List of skill names

    nurse_names : list
        List of nurse names

    Returns
    -------
    Dictionaries containing coverage requirements for the week as well as shift request data
    """
    if not os.path.isdir(os.path.join(DATAPATH, scen_name)):
        raise ValueError('No data folder found at {}'.format(DATAPATH))

    with open(os.path.join(path, filename + '.txt'), 'r') as f:
        raw_data = f.readlines()

    coverage = {}

    D = range(7)
    day_names = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

    # print(('sc = ', scen_name, 'path = ', path, 'filename = ', filename, 'S = ', S, 'K = ', K, 'N = ', N, 'shift names = ', shift_names, 'skills = ', skills, 'nurse_names = ', nurse_names))
    for i in range(len(S) * len(K)):
        cover_info = raw_data[4 + i].strip().split()
        # print('Cover info = ', cover_info)
        # print(cover_info)
        nurse = cover_info[0]
        skill = cover_info[1]
        for d in D:
            coverage[shift_names.index(nurse), skills.index(skill), d] = eval(cover_info[2 + d])
        
    num_requests = int(raw_data[5 + len(S) * len(K)].strip().split()[-1])
    requests = {(n, s, d): 0 for n in N for s in S for d in D}
    for i in range(num_requests):
        request_info = raw_data[6 + len(S) * len(K)].strip().split()
        nurse = nurse_names.index(request_info[0])
        shift_type = request_info[1]
        if shift_type == 'Any':
            shift_list = shift_names
        else:
            shift_list = [shift_type]
        day_name = request_info[2]
        day = day_names.index(day_name)
        
        for s in shift_list:
            requests[nurse, shift_names.index(s), day] = 1
        
    return coverage, requests

def read_hist(filepath, filename, S, N, shift_names):
    """
    Read the history data from the previous week

    Parameters
    ----------
    filepath : str
        Path of the file containing the data

    filename : str
        Name of the data file

    S : list
        List of shifts

    N : list
        List of nurses

    shift_names : list
        List of string names of shifts

    Returns
    -------
    tot_assignments: List of total assignments of each nurse so far

    tot_weekend: List of total weekends worked by each nurse so far

    shift_hist: List of the last shift type worked in previous week by each nurse

    work_len: Length of work stretch ending on last day of previous week for each nurse (0 if none)

    break_len: Length of break stretch ending on last day of previous week for each nurse (0 if none)
    """
    if not os.path.isdir(filepath):
        raise ValueError('No data folder found at {}'.format(filepath))

    with open(os.path.join(filepath, filename + '.txt'), 'r') as f:
        raw_data = f.readlines()

    tot_ass = []
    tot_weekend = []
    shift_hist = []
    shift_len = []
    work_len = []
    break_len = []

    for n in N:
        hist_info = raw_data[4 + n].strip().split()
        tot_ass.append(int(hist_info[1]))
        tot_weekend.append(int(hist_info[2]))
        shift_hist.append(shift_names.index(hist_info[3]))
        shift_len.append(int(hist_info[4]))
        work_len.append(int(hist_info[5]))
        break_len.append(int(hist_info[6]))

    return tot_ass, tot_weekend, shift_hist, shift_len, work_len, break_len


def write_hist(path, filename, scen_name, week, S, N, K, nurse_names, shift_names, tot_ass, \
                tot_weekend, last_shift, shift_stretch, work_stretch, break_stretch):
    """
    Write relevant history data for current week to file

    Parameters
    ----------
    path : str
        Path of the folder containing data

    filename : str 
        Name of the file we are writing the history data to

    scen_name : str
        Name of the scenario instance

    week : int
        Week number

    S : list
        List of shifts

    N : list
        List of nurses

    K : list
        List of skills

    nurse_names : list
        List of string names of nurses

    shift_names : list
        List of string names of shifts

    tot_ass : list
        List of total assignments for each nurse

    last_shift : list
        List of string names of the last shifts worked by each nurse

    shift_stretch : list
        List of the length of final shift stretch for each nurse

    work_stretch : list
        List of the length of final work stretch for each nurse

    break_stretch : list
        List of the length of the final break stretch for each nurse
    """
    if not os.path.isdir(path):
        raise ValueError('No data folder found at {}'.format(path))
    with open(os.path.join(path, filename), 'w') as f:
        lines = 'HISTORY\n'
        lines += '{} {}\n\nNURSE_HISTORY\n'.format(week, scen_name)
        for n in N:
            lines += '{} {} {} {} {} {} {}\n'.format(nurse_names[n], int(tot_ass[n]), \
                int(tot_weekend[n]), last_shift[n], shift_stretch[n], \
                    work_stretch[n], break_stretch[n])
        f.writelines(lines)



def main():
    scen = 'n035w8'
    hist_num = 0
    week_nums = [6, 2, 9, 8, 7, 7, 9, 8]
    sol_folder = 'Solution to H_{}-WD_{}'.format(hist_num, week_nums[0])
    for i in range(1, len(week_nums)):
        sol_folder += '-{}'.format(week_nums[i])

    # check if paths correct/exist- if not, make new path
    orig_path = os.path.join(DATAPATH, scen)
    new_path = os.path.join(orig_path, 'Rep Sols', sol_folder)
    
    W, N, S, K, skills, shift_names, shift_details, banned, work_blocks, \
            shift_blocks, break_blocks, contract_types, max_weekends, \
            complete_weekend, nurse_names, nurse_contracts, nurse_skills, orig_shift_blocks, orig_break_blocks = read_scenario('n035w8', 'Sc-n035w8')
    for w in W:
        tot_ass, tot_weekend, shift_hist, shift_len, work_len, break_len = read_hist(new_path, 'H{}-n035w8'.format(w), S, N, shift_names)

if __name__ == '__main__':
    main()
