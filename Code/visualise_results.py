import xlrd
import os
import numpy as np
import matplotlib.pyplot as plt

filepath = os.path.join("C:", "\\Users", "CurrB", "OneDrive", "Documents", "2020 Honours", "INRC", "Data", "Hidden", "Hidden-TXT", "Benchmarking")
filename = "ranks.xlsx"
wb = xlrd.open_workbook(os.path.join(filepath, filename))
sheet = wb.sheet_by_index(0)

ranks = {}
fig, ax = plt.subplots()

labels = []
for col in range(28, 36):
    name = sheet.col_values(col)[3]
    ranks[name] = sheet.col_values(col)[4:64]
    labels.append(name)

data = [ranks[name] for name in labels]

ax.boxplot(data)

for i in range(len(labels)):
    x = [i + 1] * len(data[i])
    ax.plot(x, data[i], "r.", alpha = 0.2)

ax.set_xlabel("Competition Model")
ax.set_ylabel("Average Rank")
ax.set_xticklabels(labels, rotation = 45, fontsize = 8)
ax.yaxis.grid(True, linestyle = "-", which = "major", color = "lightgrey", alpha = 0.5)
ax.set_title("Comparison of average instance rank for INRC-II models")

plt.tight_layout()

plt.show()


    